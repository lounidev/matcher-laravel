<!doctype html>
<html lang="en">
    <head>
        <title>My College Roomie</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <base href="{{asset('/')}}"> @yield('before-base-css')
        <link href="{{asset(mix('css/app.css'))}}" rel="stylesheet">
        <link rel="shortcut icon" type="image/png" href="{{asset('images/favicon.png')}}" /> @yield('before-base-js')
        <script type="text/javascript">
            if (window.devicePixelRatio > 1) { document.cookie = 'HTTP_IS_RETINA=1;path=/'; }
            var googleReCaptchaSiteKey = '<?php echo config('app.recaptcha_site_key'); ?>';
        </script>
        @yield('after-base-js')
        <style> [v-cloak] { display: none; } </style>
        <style type="text/css">
            :root {
                --primary-color:#0067A9;
                --secondary-color:#CF8D2E;
            };
        </style>
        <script src='https://www.google.com/recaptcha/api.js'></script>
    </head>
    <body>
        <div id="app" class="frontlayout">
            <vue-progress-bar></vue-progress-bar>
            <c-header v-if="!$route.meta.noHeader"></c-header>
            <div class="main-panel">
                <router-view></router-view>
            </div>
        </div>
        <script src="{{asset(mix('js/app.js'))}}"></script>
    </body>
</html>
