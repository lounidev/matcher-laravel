@component('mail::message')
<div>
   
    "{{ $data->title }}" report has been shared with you. Kindly login to view the report.
   
</div>

@component('mail::button', ['url' => config('app.url')])
Login Now
@endcomponent

@endcomponent

