@component('mail::message')

Sanction(s) has been imposed on you.

{{  $data['fine_ids'] ? 'As a result of the sanction you are required to pay a fine':'' }} 

{{ $data['details'] }}

<div>
	If you wish to appeal against this sanction <a href="{!! $url !!}">Click here</a>.
</div>


@endcomponent

