@component('mail::message')
<div>

Name: {{$data['first_name'].' '.$data['last_name']}} <br>
Email Address: {{$data['email']}}<br><br>
Message: {{$data['message']}} <br><br>
{{  !empty($data['reason']) ? 'Reason:': '' }} {{!empty($data['reason']) ? $data['reason']:''}}
  
</div>


@endcomponent

