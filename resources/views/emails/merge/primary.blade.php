@component('mail::message')
<div>
	
<strong>Merge Case</strong>
</div>

The case that you were assigned a Case Officer of has been merged with Case - <strong>{{ $data->case_number }}</strong>.

@component('mail::button', ['url' => config('app.url')])
Click on the link below to view the newly created case.
@endcomponent

@endcomponent
