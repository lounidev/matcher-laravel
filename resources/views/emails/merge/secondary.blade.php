@component('mail::message')
<div>
	
<strong>Merge Case</strong>
</div>

Case has been merged and this case - <strong>{{ $data->case_number }}</strong>. has been discarded.

@component('mail::button', ['url' => config('app.url')])
Click on the link below to view the newly created case.
@endcomponent

@endcomponent
