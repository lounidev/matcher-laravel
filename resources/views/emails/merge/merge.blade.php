@component('mail::message')
<div>
	
<strong>Merge Case</strong>
</div>

<?php echo $message ?> 

@component('mail::button', ['url' => config('app.url')])
Click on the link below to view the newly created case.
@endcomponent

@endcomponent
