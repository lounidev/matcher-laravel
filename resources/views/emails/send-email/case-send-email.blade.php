@component('mail::message')
<div>
	Dear, {{ $user }}
</div>

<div style="white-space: pre-wrap; font-size: 14px;">{{ $message ? $message : ''  }}</div>

@component('mail::button', ['url' => config('app.url')])
Please login to see further details
@endcomponent

@endcomponent
