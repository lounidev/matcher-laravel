<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title></title>
    </head>
    <body>
        <img src="{{url($pdf_data->display_settings->logo)}}" alt="" height="40px" style="float: right"> 
        <table class="table table-bordered">
            <tr>
                <td>
                    {{$pdf_data->display_settings->name}}                 
                </td>
            </tr>
            <tr>
                <td>
                    To:
                </td>
                <td>
                    {{ is_array($pdf_data->to)? implode(", ",$pdf_data->to):$pdf_data->to }}
                </td>
            </tr>
            <tr>
                <td>
                    Subject:
                </td>
                <td>
                    {{ $pdf_data->subject }}
                </td>
            </tr>
            <tr>
                <td>
                    Message:
                </td>
                <td>
                    {!! str_replace('[[CASE_URL]]', '', $pdf_data->message) !!}                
                </td>
            </tr>
            <tr>
                <td>
                    Regards:
                </td>
                <td>
                    {{ $pdf_data->sender_details->first_name.' '.$pdf_data->sender_details->last_name }}
                </td>
            </tr>      
        </table>
    </body>
</html>