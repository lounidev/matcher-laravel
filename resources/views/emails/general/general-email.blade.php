@component('mail::message')
<div>
    We're excited to welcome you to to UMKC.
</div>

@component('mail::button', ['url' => ''])
Login Now
@endcomponent

@endcomponent

