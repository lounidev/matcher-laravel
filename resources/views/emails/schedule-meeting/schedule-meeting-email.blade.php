@component('mail::message')
<div>
	
<strong>{{  $data['meeting_title'] }}</strong>
</div>

<div>
Meeting has been called by {{ $data['hearing_type'] }} scheduled on {{ $data['hearing_location'] }} , at {{ date('M d, y', strtotime($data['meeting_date'])) }}.
      The meeting will be conducted from {{ $data['meeting_time']['from']['hh'].':'.$data['meeting_time']['from']['mm'] }} till {{ $data['meeting_time']['to']['hh'].':'.$data['meeting_time']['to']['mm'] }}.  
</div>
<div>
    {{ !empty($data['additional_note']) ? $data['additional_note'] : '' }}
</div>
@component('mail::button', ['url' => config('app.url')])
Click here to view the meeting details in portal
@endcomponent

@endcomponent

