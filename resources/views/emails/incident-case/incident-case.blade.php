@component('mail::message')
<div>
	{{$user}}, {{ $message ? $message : ''  }}
</div>

@if(!empty($url))
	@component('mail::button', ['url' => config('app.url')])
	LOGIN
	@endcomponent
@endif
@endcomponent
