@component('mail::message')
<div>
	{{ $message ? $message : ''  }}
</div>

Reported by: <b>{{$reported_by}}</b> <br>
Incident Type: <b>{{$incident_type}}</b> <br>
Incident Report ID: <b>{{$form_id}}</b> <br>


@component('mail::button', ['url' => config('app.url')])
LOGIN
@endcomponent

@endcomponent
