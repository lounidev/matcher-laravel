<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'pending' => 'Your account verification is pending, please check your email for the activation link we sent and follow the instructions to enable your account.',
    'blocked' => 'Your account is inactive. Please contact us for further details.',
    'notuser' => 'Your are not a user.',
    'notadmin' => 'Your are not a admin.',

];
