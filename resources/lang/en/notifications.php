<?php

return [

    'create' => 
        [
            'appeal' => '<strong>:full_name</strong> has appealed against <a href=":url" role="button">Case #:case_number</a>',

            'charge' => 'You have been charged in <a href=":url" role="button">Case #:case_number<a>',

            'plea' => '<strong>:full_name</strong> has imposed a plea on <a href=":url" role="button">Case #:case_number<a>',

            'sanction' => 'Sanction(s) has been imposed on you in <a href=":url" role="button">Case #:case_number</a>',

            'notes' => '<strong>:full_name</strong> added a new note on<a href=":url" role="button"> Case #:case_number</a>',

            'meeting' => '<strong>:full_name</strong> has scheduled meeting with you on <a href=":url" role="button">Case #:case_number</a> on :meeting_date at :meeting_time',

            'email' => '<strong>:full_name</strong> has sent you an email on <a href=":url" role="button">Case #:case_number</a>',

            'attachment' => '<strong>:full_name</strong> added a new file on <a href=":url" role="button">Case #:case_number</a>',

            'split_case' => '<strong>:full_name</strong> split the case. New <a href=":url" role="button"> Case #:case_number</a> has been created',

            'merge_case' => '<strong>:full_name</strong> merged <a href=":merge_case_url" role="button">Case #:merge_case_number</a> with <a href=":url" role="button">Case # :case_number</a>',

            'incident_reported' => 'An Incident Report has been created',

            'your_incident_report' => 'Your incident report has been successfully created',

            'cron_expected_close_date' => '<a href=":url" role="button">Case #:case_number</a> is within 24 hours of it’s assigned expected close date. Please update the status of the case or the expected close date to match it’s current state',
            
            'scheduled_report' => '<strong>:full_name</strong> has scheduled a report :report_title',
        ],

    'update' => 
        [
            'appeal' => 'Your appeal has been reviewed by <strong>:full_name</strong> for <a href=":url" role="button">Case #:case_number </a>',

            'accept-approval' => '<strong>:full_name</strong> has approved the stage change of  <a href=":url" role="button">Case #:case_number </a> from <strong>:previous_stage_name</strong> to <strong>:current_stage_name</strong> requested by <strong>:officer_name</strong>',

            'charge' => 'The charge imposed on you has been updated for <a href=":url" role="button">Case #:case_number</a>', 

            'plea' => '<strong>:full_name</strong> has updated a plea on <a href=":url" role="button">Case #:case_number<a>',

            'sanction' => 'Sanctions have been updated for <a href=":url" role="button">Case #:case_number</a>',

            'meeting' => '<strong>:full_name</strong> has updated the meeting for <a href=":url" role="button">Case #:case_number</a> on :meeting_date at :meeting_time' ,

            // 'convert_to_case' => 'Case #<strong><a href=":url" role="button">:case_number</a></strong> has been converted to a case.',

            'convert_to_case' => '<strong>:full_name</strong> converted <a href=":url" role="button">IR # :case_number</a> in to a case and assigned it to <strong>:assigned_to_name</strong>',


            'close_case' => '<a href=":url" role="button">Case #:case_number</a> has been closed by <strong>:full_name</strong>',

            // 'discard_case' => 'Case #<strong><a href=":url" role="button">:case_number</a></strong> has been discarded by <strong>:full_name</strong>',

            'discard_case' => '<strong>:full_name</strong> discarded Case # :case_number. Reason: :reason_to_discard',

            'decline-approval' => '<strong>:full_name</strong> has declined the stage change request  by <strong>:officer_name</strong> on  <a href=":url" role="button">Case #:case_number</a> from <strong>:stage</strong> to <strong>:previous_stage_name</strong>',

            'change-stage' => '<strong>:full_name</strong> has changed the stage for <a href=":url" role="button">Case #:case_number</a>',

            'change_owner' => '<a href=":url" role="button">Case #:case_number</a> has now been assigned to <strong>:assigned_to_name</strong>',

            'expected_close_date' => 'Expected close date has been updated for <a href=":url" role="button">Case #:case_number</a> by <strong>:full_name</strong>',

            'completed_sanction' => '<strong>:full_name</strong> has completed the sanction for <a href=":url" role="button">Case #:case_number</a>',

            'assign-proper-user' => 'You have been added as <strong>:person_type</strong> in <a href=":url" role="button">Case #:case_number</a>',

            'add_involved_party' =>'<strong>:user_added</strong> has been added as <strong>:person_type</strong> in <a href=":url" role="button">Case #:case_number</a>',

            'delete_involved_party' => '<strong>:user_removed</strong> has been removed as <strong>:person_type</strong> from <a href=":url" role="button">Case #:case_number</a>',

            'assign-proper-reporter' => 'You have been added as a reporter in <a href=":url" role="button">Case #:case_number</a>',

            'case_at_risk' => '<a href=":url" role="button">Case #:case_number</a> has been marked at risk by <strong>:full_name</strong>',

            'case_not_at_risk' => '<a href=":url" role="button">Case #:case_number</a> has been unmarked at risk by <strong>:full_name</strong>',

            'change_stage_mail' => 'A request to move <a href=":url" role="button">Case #:case_number</a>  to stage :stage has been requested',

            'incident_reported' => 'An Incident Report has been updated',

            'reopen_case' => ':full_name reopened <a href=":url" role="button">Case #:case_number</a>',
            'saved_report' => '<strong>:full_name</strong> has given you access of a report saved.',
            'scheduled_report' => '<strong>:full_name</strong> has updated scheduled report :report_title',
        ],
    'delete' => [

            'charge' => 'The charge imposed on you in <a href=":url" role="button">Case #:case_number</a> has been removed',

            'plea' => '<strong>:full_name</strong> has removed a plea on <a href=":url" role="button">Case #:case_number<a>',
            
            'sanction' => 'The sanction imposed on you in <a href=":url" role="button">Case #:case_number</a> has been removed',
            'scheduled_report' => ':full_name has deleted scheduled report :report_title',

    ]



];