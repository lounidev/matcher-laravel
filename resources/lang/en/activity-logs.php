<?php

return [

	'create' => 
		[
			'appeal' => ' appealed against sanction ',
			'sanction' => ' assigned sanction against ',
			'notes' => ' has added a new note.',
			'meeting' => ' has scheduled a meeting with ',
			'email' => ' has sent an email to ',
			'attachment' => ' attached following files ',
			'merge_case' => ' case merge ',
			'split_case' => ' split case ',
 		],

 	'update' => 
 		[
 			'approve_stage' => ' has approved the stage change on Case # ',
			'appeal' => ' added a review statement against the appeal ',
			'charge' => ' has updated charges',
			'decline_approval' => 'has declined the stage change request  by ',
			'sanction' => ' updated a sanction against ',
			'completed_sanction' => ' has completed sanction ',
			'discard_case' => ' has discarded case. Reason: ',
			'case_stage' => 'Case stage has been changed to ',
			'change_owner' => ' changed case owner to ',
			'change_stage_mail' => ' has requested to change the state of Case # ',
			'assign_owner' => ' assigned case to ',
			'convert_to_case' => ' converted ',
			'add_involved_parties' => ' has been added to the case as an ',
			'delete_involved_parties' => 'has deleted',
			'flag_involved_parties' => ' involved party member ',
			'expected_close_date' => ' has updated expected close date',
			'close_case' => ' has closed case',
			'split_case' => ' case split into new case',
			'assign_proper_reporter' => ' has linked reported by user to ',
			'add_related_case' => ' added a related Case # ',
			'remove_related_case' => ' removed related Case # ',
			'assign_proper_user' => ' has been added to the case as an ',
			'ir' => ' modified details of incident report ',
			'case' => ' modified details of case ',
			'reopen_case' => ' reopened this case',
 		],
 	'delete' => [
			'charge' => ' has deleted charge imposed on ',
			'sanction' => ' deleted a sanction imposed on ',

 	]



];


