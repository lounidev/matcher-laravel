import Vue from 'vue';

var componentIcons = {
    'textarea': 'icon-text-editor',
    'input':"icon-text-box",
    'radio':"icon-radio-on-button",
    'radio':"icon-radio-on-button",
    'dropdown':"icon-drop-down-list",
    'date':"icon-calendar",
    'time':"icon-clock",
    'checkbox' : "icon-change-status"
};

var personTypes = {
   1 : 'Accused',
   2 : 'Victim',
   3 : 'Witness',
   4 : 'Complaint',
   5 : 'Respondent/Offender'
};

var overAllStatsKey = {
    'ir_reported' : 'Total Incidents Reported',
    'convert_to_case' : 'Total IR Converted to Case',
    'discard_case' : 'Total Cases Discarded',
    'close_case' : 'Total Cases Closed',
    'active' : 'Total Active Cases',
    'pending' : 'Total Pending Incident Reports',
    'case_at_risk' : 'Cases at Risk',
    'involved_students' : 'Total Student Involved in Cases',
    'average_duration' : 'Avg. Case Completion Duration',
    'current_average_duration' : 'Avg. Stage Duration',
};

var tableWidgetKey = {
    'student_involved' : 'Students',
    'employee_involved' : 'Employees',
    'anonymous_involved' : 'Anonymous'
};


Vue.filter('objectToStringDetails', function (value) {
    if(!value.length){
        return 'No value found';
    }
    var namesArray = [];
    _.forEach(value, function(val, ind){
        if(val.value == true){
            namesArray.push(val.label);
        }
    });
    var namesString = namesArray.join(", ",);

    return namesString ? namesString : "N/A";
});


Vue.filter('calculatePercentageDiff', function (value){
  
    if(!value.previous_total_value && value.total_value){
        return;
    }
    if(value.previous_total_value < 1 && value.total_value){
        return;
    }
    if(!value.previous_total_value && !value.total_value){
        return;
    }
    if(value.previous_total_value && !value.total_value){
        return;
    }
    if(value.previous_total_value && value.total_value < 1){
        return;
    }
    else{
        var ans = value.total_value / value.previous_total_value * 100;

        if(ans > 100){
            var percentage = (ans-100);
            return '(' + percentage.toFixed(2) + '% more)';
        }

        if(ans < 100){
            var percentage = (100-ans);
            return '(' + percentage.toFixed(2) + '% less)';       
        }

        if(value.total_value == value.previous_total_value){
            return '(exact)';
        }

    }
});

Vue.filter('objectToStringFullName', function (value) {
    if(value[value.length-1].first_name){
        var lastItem = value[value.length-1].first_name+' '+value[value.length-1].middle_name+' '+value[value.length-1].last_name;
        var namesArray = [];
        _.forEach(value, function(val, ind){
            namesArray.push(val.first_name+' '+val.middle_name+' '+val.last_name);
        });
        var namesString = namesArray.slice(0,value.length-1).join(", ",);
        if(value.length == 1){
            return lastItem;
        }
        return (namesString + ' & ' + lastItem);
    }
    if(value[value.length-1].display_name){
        var lastItem = value[value.length-1].display_name;
        var namesArray = [];
        _.forEach(value, function(val, ind){
            namesArray.push(val.display_name);
        });
        var namesString = namesArray.slice(0,value.length-1).join(", ",);
        if(value.length == 1){
            return lastItem;
        }
        return (namesString + ' & ' + lastItem);
    }
    if(value[value.length-1].name){
        var lastItem = value[value.length-1].name;
        var namesArray = [];
        _.forEach(value, function(val, ind){
            namesArray.push(val.name);
        });
        var namesString = namesArray.slice(0,value.length-1).join(", ",);
        if(value.length == 1){
            return lastItem;
        }
        return (namesString + ' & ' + lastItem);
    }
});

Vue.filter('toTitleCase', function (str) {
    return str.replace(
        /\w\S*/g,
        function(txt) {
            if(txt == 'IX'){
                return txt;
            }
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        }
        );
});

Vue.filter('replaceSpecialCharacterWithSpace', function (str) {
    return str.split('-').join(' ').split('_').join(' ');
});



Vue.filter('formComponentIcon', function (type) {
    return componentIcons[type];
    console.log('this '+type+' is not added in componentIcon filter list in file filter.js');
});

function fullName(user) {
    if(user && Object.keys(user).length){
        if(user.first_name){
            return user.first_name +' '+ user.middle_name+' '+ user.last_name
        }else if(user.full_name){
            return user.full_name
        }
    }
}

Vue.filter('fullName', function (user,status) {
    return fullName(user);
});

Vue.filter('attachmentIconUrl', function (value) {
    var getIndex = value.split('.');
    var iconPostfix = getIndex[getIndex.length-1];
    var iconPath = 'images/files/';

    if(iconPostfix =='docx' || iconPostfix =='doc'){
        iconPath  += 'doc';
    }
    else if(iconPostfix =='pptx' || iconPostfix =='ppt'){
        iconPath  += 'ppt';
    }
    else if(iconPostfix =='jpg' || iconPostfix =='jpeg'){
        iconPath  += 'jpg';
    }
    else if(iconPostfix =='xlsx' || iconPostfix =='xls'){
        iconPath  += 'xls';
    }
    else{
        iconPath  += iconPostfix;
    }

    iconPath += '.png';

    // console.log(iconPath , 12312);

    return iconPath;
});


Vue.filter('personType', function (type) {
    return personTypes[type];
});

// formatted datetime in local timezone
Vue.filter('fullDateTimeTimezone', function (dateTime) {
    var offset = moment().utcOffset();
    var localTime = moment.utc(dateTime).utcOffset(offset).format('MMM D, YYYY') +' at ' +  moment.utc(dateTime).utcOffset(offset).format('h:mm a');
    return localTime;
});

Vue.filter('description', function (value) {
    var namesArray = [];
    if(value && Object.keys(value).length){
      _.forEach(value, function(val,ind){
        namesArray.push(val.name);
      })
    }
   return namesArray.join(", ");
});
Vue.filter('arrayToStringNames', function (value) {
    if(!value || !value.length){
        return 'No recipients selected';
    }else{
        var lastItem = value[value.length-1];
        var namesArray = value;
        var namesString = namesArray.slice(0,value.length-1).join(", ",);
        if(value.length == 1){
            return lastItem;
        }
        return (namesString + ' & ' + lastItem);
    }
});

Vue.filter('objectToStringNames', function (value) {
    if(!value.length){
        return 'No recipients selected';
    }else{
        var lastItem = value[value.length-1].full_name ;
        var namesArray = [];
        _.forEach(value, function(val, ind){
            namesArray.push(val.full_name);
        });
        var namesString = namesArray.slice(0,value.length-1).join(", ",);
        if(value.length == 1){
            return lastItem;
        }
        return (namesString + ' & ' + lastItem);
    }
});

Vue.filter('dateString', function (dateTime) {
    return moment(dateTime).format('MMM D, YYYY');
});

Vue.filter('dateDifference', function (period) {
    var start_date = moment(period.from);
    var end_date = moment(period.to);
    return end_date.diff(start_date, 'days');
});

Vue.filter('formatTime', function (value) {
    return moment(value).format('HH:mm a');
});

function formatDate(value){
    if(value){
        return moment(value).format('MMM DD, YYYY');
    }
    return '-';
}

Vue.filter('formatDate', function(value) {
  if (value) {
    return formatDate(value);
  }
});

Vue.filter('attachmentName', function(value) {
    var fileName = [];
    if(value.length){
        _.forEach(value,function(val,key){
            fileName.push(val.display_name);
        });
        return fileName.join(', ');
    }else{
        return 'No Attachment found';
    }
});

Vue.filter('fullDateTime', function (dateTime) {
    if(dateTime){
        return moment(dateTime).format('MMM D, YYYY') +' at ' +  moment(dateTime).format('h:mm a');
    }
    return '-';
}); 

function valueForSystemLogs(value) {
    if (value.instance_type == 'stage') {
        var difference = JSON.parse(JSON.parse(value.difference));
        return difference.stage.new_value;
    }
    if (value.instance_type == 'expected_close_date') {
        var difference = JSON.parse(value.difference);
        return formatDate(difference.expected_close_date);
    }
    if(value.instance_type =='case_at_risk'){
        var difference = JSON.parse(value.difference);
        if(difference.at_risk == 1){
            return ' marked Case # '+value.case_number+' at risk';
        }else{
            return ' removed Case # '+value.case_number+' from being at risk';
        }
    }
    if (value.instance_type == 'close_case') {
        var difference = JSON.parse(value.difference);
        return difference.case_number;
    }
    if (value.instance_type == 'discard_case') {
        var difference = JSON.parse(value.difference);
        return ""+difference.case_number+"" +' due to '+ ""+difference.reason_to_discard+"";
    }
    if(value.instance_type =='notes'){
        var difference = JSON.parse(value.difference);
        if(difference.privacy == 'public'){
            return ' added a new public note ';
        }else{
            return ' added a new private note ';
        }
    }
    if (value.instance_type == 'attachment') {
        var difference = JSON.parse(value.difference);
        var imageName = '';
        for (var i = 0; i < difference.file_data.length; i++) {
            if((i + 1) == (difference.file_data.length)){
                imageName += difference.file_data[i].display_name;
            }else{
                imageName += difference.file_data[i].display_name+', ';
            }
        }
        return imageName;
    }
    if (value.instance_type == 'meeting') {
        var difference = JSON.parse(value.difference);
        var name = '';
        for (var i = 0; i < difference.invitees.length; i++) {
            if((i + 1) == (difference.invitees.length)){
                name += difference.invitees[i].first_name+' '+difference.invitees[i].last_name;
            }else{
                name += difference.invitees[i].first_name+' '+difference.invitees[i].last_name+', ';
            }
        }
        return name;
    }
    if (value.instance_type == 'user_custom_fields') {
        if(value.action == 'create'){
            var difference = JSON.parse(value.difference);
            return ""+difference.label.new_value+"";
        }
    }
    if (value.instance_type == 'incident_types') {
        if(value.action == 'create'){
            var difference = JSON.parse(value.difference);
            return ""+difference.name.new_value+"";
        }else if(value.action == 'update'){
            var difference = JSON.parse(value.difference);
            if(difference.is_archived){
                return ' is archived the incident type ';
            }else{
                return ' updated the incident type from ' + ""+difference.name.old_value+"" +' to '+ ""+difference.name.new_value+"";    
            }
        }
    }
    if (value.instance_type == 'locations') {
        if(value.action == 'create'){
            var difference = JSON.parse(value.difference);
            return ' has added a new location - ' + ""+difference.name.new_value+"";
        }else if(value.action == 'update'){
            var difference = JSON.parse(value.difference);
            if(difference.is_archived){
                return ' is archived the location ';
            }else{
                return ' updated the location from ' + ""+difference.name.old_value+"" +' to '+ ""+difference.name.new_value+"";    
            }
        }
    }
    if (value.instance_type == 'violations') {
        if(value.action == 'create'){
            var difference = JSON.parse(value.difference);
            return ' has added a new violation - ' + ""+difference.name.new_value+"";
        }else if(value.action == 'update'){
            var difference = JSON.parse(value.difference);            
            if(difference.is_archived){
                return ' is archived the violation ';
            }else{
                return ' updated the violation from ' + ""+difference.name.old_value+"" +' to '+ ""+difference.name.new_value+"";    
            }
        }
    }
    if (value.instance_type == 'plea_types') {
        if(value.action == 'create'){
            var difference = JSON.parse(value.difference);
            return ' has added a new plea type - ' + ""+difference.name.new_value+"";
        }else if(value.action == 'update'){
            var difference = JSON.parse(value.difference);            
            if(difference.is_archived){
                return ' is archived the plea type ';
            }else{
                return ' updated the plea type from ' + ""+difference.name.old_value+"" +' to '+ ""+difference.name.new_value+"";    
            }
        }
    }
    if (value.instance_type == 'sanctions') {
        if(value.action == 'create'){
            var difference = JSON.parse(value.difference);
            return ' has added a new sanction - ' + ""+difference.name.new_value+"";
        }else if(value.action == 'update'){
            var difference = JSON.parse(value.difference);            
            if(difference.is_archived){
                return ' is archived the sanction ';
            }else{
                return ' updated the sanction from ' + ""+difference.name.old_value+"" +' to '+ ""+difference.name.new_value+"";    
            }
        }
    }
    if (value.instance_type == 'fines') {
        if(value.action == 'create'){
            var difference = JSON.parse(value.difference);
            return ' has added a new fine - ' + ""+difference.name.new_value+"";
        }else if(value.action == 'update'){
            var difference = JSON.parse(value.difference);            
            if(difference.is_archived){
                return ' is archived the fine ';
            }else{
                return ' updated the fine from ' + ""+difference.name.old_value+"" +' to '+ ""+difference.name.new_value+"";    
            }
        }
    }
    if (value.instance_type == 'appeal_types') {
        if(value.action == 'create'){
            var difference = JSON.parse(value.difference);
            return ' has added a new appeal type - ' + ""+difference.name.new_value+"";
        }else if(value.action == 'update'){
            var difference = JSON.parse(value.difference);            
            if(difference.is_archived){
                return ' is archived the appeal type ';
            }else{
                return ' updated the appeal type from ' + ""+difference.name.old_value+"" +' to '+ ""+difference.name.new_value+"";    
            }
        }
    }
    if (value.instance_type == 'appeal') {
        if(value.action == 'create'){
            return ' appealed against the sanction ';
        }else if(value.action == 'update'){
            return ' updated the appeal against the sanction ';
        }
    }
    if (value.instance_type == 'appeal_statuses') {
        if(value.action == 'create'){
            var difference = JSON.parse(value.difference);
            return ' has added a new appeal status - ' + ""+difference.name.new_value+"";
        }else if(value.action == 'update'){
            var difference = JSON.parse(value.difference);            
            if(difference.is_archived){
                return ' is archived the appeal status ';
            }else{
                return ' updated the appeal status from ' + ""+difference.name.old_value+"" +' to '+ ""+difference.name.new_value+"";    
            }
        }
    }
    if (value.instance_type == 'appeal_review_types') {
        if(value.action == 'create'){
            var difference = JSON.parse(value.difference);
            return ' has added a new appeal review - ' + ""+difference.name.new_value+"";
        }else if(value.action == 'update'){
            var difference = JSON.parse(value.difference);            
            if(difference.is_archived){
                return ' is archived the appeal review ';
            }else{
                return ' updated the appeal review from ' + ""+difference.name.old_value+"" +' to '+ ""+difference.name.new_value+"";    
            }
        }
    }
    if (value.instance_type == 'appeal_outcomes') {
        if(value.action == 'create'){
            var difference = JSON.parse(value.difference);
            return ' has added a appeal outcome - ' + ""+difference.name.new_value+"";
        }else if(value.action == 'update'){
            var difference = JSON.parse(value.difference);            
            if(difference.is_archived){
                return ' is archived the appeal outcome ';
            }else{
                return ' updated the appeal outcome from ' + ""+difference.name.old_value+"" +' to '+ ""+difference.name.new_value+"";    
            }
        }
    }
    if (value.instance_type == 'board_member_types') {
        if(value.action == 'create'){
            var difference = JSON.parse(value.difference);
            return ' has added a board member type - ' + ""+difference.name.new_value+"";
        }else if(value.action == 'update'){
            var difference = JSON.parse(value.difference);            
            if(difference.is_archived){
                return ' is archived the board member type ';
            }else{
                return ' updated the board member type from ' + ""+difference.name.old_value+"" +' to '+ ""+difference.name.new_value+"";    
            }
        }
    }
    if (value.instance_type == 'hearing_types') {
        if(value.action == 'create'){
            var difference = JSON.parse(value.difference);
            return ' has added a hearing type - ' + ""+difference.name.new_value+"";
        }else if(value.action == 'update'){
            var difference = JSON.parse(value.difference);            
            if(difference.is_archived){
                return ' is archived the hearing type ';
            }else{
                return ' updated the hearing type from ' + ""+difference.name.old_value+"" +' to '+ ""+difference.name.new_value+"";    
            }
        }
    }
    if (value.instance_type == 'hearing_locations') {
        if(value.action == 'create'){
            var difference = JSON.parse(value.difference);
            return ' has added a hearing location - ' + ""+difference.name.new_value+"";
        }else if(value.action == 'update'){
            var difference = JSON.parse(value.difference);            
            if(difference.is_archived){
                return ' is archived the hearing location ';
            }else{
                return ' updated the hearing location from ' + ""+difference.name.old_value+"" +' to '+ ""+difference.name.new_value+"";    
            }
        }
    }
    if (value.instance_type == 'student_flags') {
        if(value.action == 'create'){
            var difference = JSON.parse(value.difference);
            return ' has added a flag status - ' + ""+difference.name.new_value+"";
        }else if(value.action == 'update'){
            var difference = JSON.parse(value.difference);            
            if(difference.is_archived){
                return ' is archived the flag status ';
            }else{
                return ' updated the flag status from ' + ""+difference.name.old_value+"" +' to '+ ""+difference.name.new_value+"";    
            }
        }
    }
    if (value.instance_type == 'display_settings') {
        if(value.action == 'update'){
            var difference = JSON.parse(value.difference);            
                return ' updated Display Settings';    
        }
    }
    if (value.instance_type == 'assign-proper-reporter') {
        if(value.action == 'update'){
            return ' has linked reported by user to  ';           
        }
    }
}

Vue.filter('systemLogsMessages', function (value) {

    var fullMessage = '';

    if(value.instance_type =='stage' && value.action =='update'){
        return "<span>"+fullName(value.user)+" moved Case # "+value.case_number+" to "+valueForSystemLogs(value)+"</span>";
    }
    else if(value.instance_type =='expected_close_date' && value.action =='update'){
        return "<span>"+fullName(value.user)+" updated the expected close date to "+valueForSystemLogs(value)+"</span>";
    }
    else if(value.instance_type =='change_owner' && value.action =='update'){
        //console.log(value , 12312);
        return "<span>"+fullName(value.user)+" assigned <user> as case owner</span>";
    }
    else if(value.instance_type =='delete_involved_party' && value.action =='update'){
        return "<span>"+fullName(value.user)+" disassociated <user> from case</span>";
    }
    else if(value.instance_type =='flag_involved_party' && value.action =='update'){
        return "<span>"+fullName(value.user)+" flagged involved party member <user></span>";
    }
    else if(value.instance_type =='add_involved_party' && value.action =='update'){
        return "<span>"+fullName(value.user)+" has been added to the case as </span>";
    }
    else if(value.instance_type =='case_at_risk' && value.action =='update'){
        return "<span>"+fullName(value.user)+""+valueForSystemLogs(value)+"</span>";
    }
    else if(value.instance_type =='charge' && value.action =='delete'){
        return "<span>"+fullName(value.user)+" deleted charges imposed on user</span>";
    }
    else if(value.instance_type =='charge' && value.action =='update'){
        return "<span>"+fullName(value.user)+" updated charges imposed on user</span>";
    }
    else if(value.instance_type =='charge' && value.action =='create'){
        return "<span>"+fullName(value.user)+" charged user with Violation</span>";
    }
    else if(value.instance_type =='sanction' && value.action =='delete'){
        return "<span>"+fullName(value.user)+" deleted a sanction imposed on user</span>";
    }
    else if(value.instance_type =='sanction' && value.action =='update'){
        return "<span>"+fullName(value.user)+" updated a sanction against user</span>";
    }
    else if(value.instance_type =='sanction' && value.action =='create'){
        return "<span>"+fullName(value.user)+" issued a sanction against user</span>";
    }
    else if(value.instance_type =='appeal' && value.action =='update'){
        return "<span>"+fullName(value.user)+" updated an appeal against a sanction</span>";
    }
    else if(value.instance_type =='close_case' && value.action =='update'){
        return "<span>"+fullName(value.user)+" has closed Case # "+valueForSystemLogs(value)+"</span>";
    }
    else if(value.instance_type =='discard_case' && value.action =='update'){
        return "<span>"+fullName(value.user)+" has discarded Case # "+valueForSystemLogs(value)+"</span>";
    }
    else if(value.instance_type =='notes' && value.action =='create'){
        return "<span>"+fullName(value.user)+""+valueForSystemLogs(value)+"</span>";
    }
    else if(value.instance_type =='attachment' && value.action =='create'){
        return "<span>"+fullName(value.user)+" uploaded "+valueForSystemLogs(value)+"</span>";
    }
    else if(value.instance_type =='email' && value.action =='create'){
        return "<span>A private email was sent by "+fullName(value.user)+"</span>";
    }
    else if(value.instance_type =='meeting' && value.action =='create'){
        return "<span>"+fullName(value.user)+" has scheduled a meeting with "+valueForSystemLogs(value)+"</span>";
    }
    else if(value.instance_type =='user_custom_fields' && value.action =='create'){
        return "<span>"+fullName(value.user)+" added a custom field - "+valueForSystemLogs(value)+"</span>";
    }
    else if(value.instance_type =='user_profiles' && value.action =='update'){
        return "<span>"+fullName(value.user)+" updated the additional details</span>";
    }
    else if(value.instance_type =='users' && value.action =='update'){
        return "<span>"+fullName(value.user)+" has bookmarked a student</span>";
    }
    else if(value.instance_type =='incident_types' && value.action =='create'){
        return "<span>"+fullName(value.user)+" created a new incident type - "+valueForSystemLogs(value)+"</span>";
    }
    else if(value.instance_type =='incident_types' && value.action =='update'){
        return "<span>"+fullName(value.user)+""+valueForSystemLogs(value)+"</span>";
    }
    else if(value.instance_type =='checklists' && value.action =='create'){
        return "<span>"+fullName(value.user)+" created a new checklist items</span>";
    }
    else if(value.instance_type =='stages' && value.action =='create'){
        return "<span>"+fullName(value.user)+" created a new workflows</span>";
    }
    else if(value.instance_type =='stages' && value.action =='update'){
        return "<span>"+fullName(value.user)+" updated a workflows</span>";
    }
    else if(value.instance_type =='checklists' && value.action =='update'){
        return "<span>"+fullName(value.user)+" updated a checklist items</span>";
    }
    else if(value.instance_type =='locations' && value.action =='create'){
        return "<span>"+fullName(value.user)+""+valueForSystemLogs(value)+"</span>";
    }
    else if(value.instance_type =='locations' && value.action =='update'){
        return "<span>"+fullName(value.user)+""+valueForSystemLogs(value)+"</span>";
    }
    else if(value.instance_type =='violations' && value.action =='create'){
        return "<span>"+fullName(value.user)+""+valueForSystemLogs(value)+"</span>";
    }
    else if(value.instance_type =='violations' && value.action =='update'){
        return "<span>"+fullName(value.user)+""+valueForSystemLogs(value)+"</span>";
    }
    else if(value.instance_type =='plea_types' && value.action =='create'){
        return "<span>"+fullName(value.user)+""+valueForSystemLogs(value)+"</span>";
    }
    else if(value.instance_type =='plea_types' && value.action =='update'){
        return "<span>"+fullName(value.user)+""+valueForSystemLogs(value)+"</span>";
    }
    else if(value.instance_type =='sanctions' && value.action =='create'){
        return "<span>"+fullName(value.user)+""+valueForSystemLogs(value)+"</span>";
    }
    else if(value.instance_type =='sanctions' && value.action =='update'){
        return "<span>"+fullName(value.user)+""+valueForSystemLogs(value)+"</span>";
    }
    else if(value.instance_type =='fines' && value.action =='create'){
        return "<span>"+fullName(value.user)+""+valueForSystemLogs(value)+"</span>";
    }
    else if(value.instance_type =='fines' && value.action =='update'){
        return "<span>"+fullName(value.user)+""+valueForSystemLogs(value)+"</span>";
    }
    else if(value.instance_type =='appeal' && value.action =='create'){
        return "<span>"+fullName(value.user)+""+valueForSystemLogs(value)+"</span>";
    }
    else if(value.instance_type =='appeal' && value.action =='update'){
        return "<span>"+fullName(value.user)+""+valueForSystemLogs(value)+"</span>";
    }
    else if(value.instance_type =='appeal_types' && value.action =='create'){
        return "<span>"+fullName(value.user)+""+valueForSystemLogs(value)+"</span>";
    }
    else if(value.instance_type =='appeal_types' && value.action =='update'){
        return "<span>"+fullName(value.user)+""+valueForSystemLogs(value)+"</span>";
    }
    else if(value.instance_type =='appeal_statuses' && value.action =='create'){
        return "<span>"+fullName(value.user)+""+valueForSystemLogs(value)+"</span>";
    }
    else if(value.instance_type =='appeal_statuses' && value.action =='update'){
        return "<span>"+fullName(value.user)+""+valueForSystemLogs(value)+"</span>";
    }
    else if(value.instance_type =='appeal_review_types' && value.action =='create'){
        return "<span>"+fullName(value.user)+""+valueForSystemLogs(value)+"</span>";
    }
    else if(value.instance_type =='appeal_review_types' && value.action =='update'){
        return "<span>"+fullName(value.user)+""+valueForSystemLogs(value)+"</span>";
    }
    else if(value.instance_type =='appeal_outcomes' && value.action =='create'){
        return "<span>"+fullName(value.user)+""+valueForSystemLogs(value)+"</span>";
    }
    else if(value.instance_type =='appeal_outcomes' && value.action =='update'){
        return "<span>"+fullName(value.user)+""+valueForSystemLogs(value)+"</span>";
    }
    else if(value.instance_type =='board_member_types' && value.action =='create'){
        return "<span>"+fullName(value.user)+""+valueForSystemLogs(value)+"</span>";
    }
    else if(value.instance_type =='board_member_types' && value.action =='update'){
        return "<span>"+fullName(value.user)+""+valueForSystemLogs(value)+"</span>";
    }
    else if(value.instance_type =='hearing_types' && value.action =='create'){
        return "<span>"+fullName(value.user)+""+valueForSystemLogs(value)+"</span>";
    }
    else if(value.instance_type =='hearing_types' && value.action =='update'){
        return "<span>"+fullName(value.user)+""+valueForSystemLogs(value)+"</span>";
    }
    else if(value.instance_type =='hearing_locations' && value.action =='create'){
        return "<span>"+fullName(value.user)+""+valueForSystemLogs(value)+"</span>";
    }
    else if(value.instance_type =='hearing_locations' && value.action =='update'){
        return "<span>"+fullName(value.user)+""+valueForSystemLogs(value)+"</span>";
    }
    else if(value.instance_type =='student_flags' && value.action =='create'){
        return "<span>"+fullName(value.user)+""+valueForSystemLogs(value)+"</span>";
    }
    else if(value.instance_type =='student_flags' && value.action =='update'){
        return "<span>"+fullName(value.user)+""+valueForSystemLogs(value)+"</span>";
    }else if(value.instance_type =='display_settings' && value.action =='update'){
        return "<span>"+fullName(value.user)+""+valueForSystemLogs(value)+"</span>";
    }else if(value.instance_type =='assign-proper-reporter' && value.action =='update'){
        return "<span>"+fullName(value.user)+""+valueForSystemLogs(value)+"</span>";
    }

});


Vue.filter('overAllStatsKeyValue', function (key) {
    return overAllStatsKey[key];
}); 

Vue.filter('tableWidgetKeyValue', function (key) {
    return tableWidgetKey[key] ? tableWidgetKey[key] : key;
}); 

Vue.filter('removePrefix', function(value){
    if(!value.includes('.')){
        if(value.includes('_id')){
            return value.split('_id')[0];
        }
        return value;
    }

    if(value.includes('_id')){
        return value.split(".")[1].split('_id')[0];
    }
    return value.split(".")[1];
});

Vue.filter('availableUsersLabel', function(value){
    var label = '';
    if(value.send_to == 'me' || value.availability == 'me'){
        label = 'Only Me';
        return label;
    }
    if(value.send_to == 'specific_users' || value.availability == 'specific_users'){
        label = 'Specific Users';
        return label;
    }

    if(value.send_to == 'incident_type_admins' || value.availability == 'incident_type_admins'){
        label = 'All Incident Type Admins';
        return label;
    }
});


Vue.filter('repeatReportLabel', function(value){
    var label = '';
    if(value.repeat == 'week'){
        label = 'Every Week';
        return label;
    }
    if(value.repeat == 'two_weeks'){
        label = 'Every 2 Week';
        return label;
    }
    if(value.repeat == 'month'){
        label = 'Every Month (Date: '+ value.date +')';
        return label;
    }
    
    return value;
});

Vue.filter('avgDurationForPerStageReports', function (avg) {
    if(avg > 1){
        return avg + " days";
    }else{
        return avg + " day";
    }
});
