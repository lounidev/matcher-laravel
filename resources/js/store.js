import Vue from 'vue';
import Vuex from 'vuex';
import reports from './store/reports';


Vue.use(Vuex);


export default new Vuex.Store({

    modules : {
        reports
    },

    // You can use it as state property
    state: {
      authUser : localStorage.getItem('user')?localStorage.getItem('user'):false,
      displaySetting : '',
      caseData:'',
      loginTab : false,
      forgotPassword : false,
      title: '',
  },

    // You can use it as a state getter function (probably the best solution)
    getters: {
      getAuthUser(state){
        return     state.authUser;
    },
    getLoginTab(state){
        return     state.loginTab;
    },
    getForgotPassword(state){
        return     state.forgotPassword;
    },
    getTitle(state){
        return     state.title;
    },
    getDisplaySetting(state){
     state.displaySetting = JSON.parse(localStorage.getItem('displaySetting'));
     return state.displaySetting;

 },
 getCaseData(state){
     state.caseData = JSON.parse(localStorage.getItem('caseData'));
     return state.caseData;

 },
},

    // Mutation for when you use it as state property
    mutations: {
      setAuthUser(state, data){
         localStorage.setItem('user', JSON.stringify(data));
         state.authUser = localStorage.getItem('user');
     },
     setLoginTab(state, data){
      state.loginTab = data;
  },
  setForgotPassword(state, data){
      state.forgotPassword = data;
  },
  setTitle(state, data){
      state.title = data;
  },
  setDisplaySetting(state, data){
   localStorage.setItem('displaySetting', JSON.stringify(data));
   state.displaySetting =data;
},
setCaseData(state, data){
   localStorage.setItem('caseData', JSON.stringify(data));
   state.caseData = data;
},
},
});