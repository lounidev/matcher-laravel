const puppeteer = require('puppeteer');

const filePath = process.argv[2];
const url = process.argv[3];
const access_token = process.argv[4];

(async () => {
    const browser = await puppeteer.launch({
        args: [
            "--no-sandbox",
            "--disable-setuid-sandbox",
        ],
    });
    const page = await browser.newPage();

    const headers = {
        Authorization: `Bearer ` + access_token
    };

    await page.setExtraHTTPHeaders(headers);

    const allPagesLoaded = new Promise(resolve => {
        page.on('response', async response => {
            if (response.url().indexOf("activity-log") >= 0) {

                const textBody = await response.text()
                const data = JSON.parse(textBody)

                if (data.pagination.current < data.pagination.last) {
                    await page.waitFor(1000)

                    const loadMoreButton = await page.$('.btn.btn-primary')
                    await loadMoreButton.click()
                } else {
                    resolve()
                }
            }
        })
        if (url.indexOf("current-situation") >= 0) {
            setTimeout(() => {
                resolve()
            }, 10000)
        }
    })

    await page.goto(url, {
        timeout: 2500000,
        waitUntil: 'networkidle2',
    });

    await allPagesLoaded;

    await page.pdf({
        format: 'A4',
        path: filePath,
        printBackground: true,
        margin: {
            top: 40
        }
    });
    await browser.close();

})();