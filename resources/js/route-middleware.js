import router from './routes';
import VueRouter from 'vue-router'
import store from './store.js'

const app = new Vue({
    router: router,
    store
});

const super_admin = 1;
const incidentTypeAdmin = 2;
const incidentOfficer = 3;
const staff = 4;
const studentStaff = 5;
const student = 6;
const allAdmins = [super_admin, incidentTypeAdmin, incidentOfficer];


const title = document.title

router.beforeEach((to, from, next) => {
    let user;
    if (router.app.$store.getters.getAuthUser != 'undefined') {
        user = JSON.parse(router.app.$store.getters.getAuthUser);
    }

    document.title = (title + ' | ' + to.meta.title)
    if (to.matched.some(record => record.meta.requiresAuth) && !router.app.$auth.isAuthenticated()) {
        next({ name: 'login' });
    } else if (!to.matched.some(record => record.meta.requiresAuth) && router.app.$auth.isAuthenticated()) {

        if (user && user.role_id == super_admin) {

            next({ name: 'conduct-incidents' });
        } else if (user && user.role_id == studentStaff) {

            next({ name: 'conduct-incidents' });
        } else if (user && user.role_id == student) {

            next({ name: 'student-portal-list-view' });
        } else if (user && user.role_id == staff) {

            next({ name: 'student-portal-list-view' });
        } else if (user && user.role_id == incidentTypeAdmin) {

            next({ name: 'conduct-incidents' });
        } 
        
    } else if(to.matched.some(record => record.meta.requiresAdmin)) {
        if(user.role_id == student){
            next({ name: 'student-portal-list-view' });
        }
        next();
    }else{

        next();

    }
})

router.afterEach((to, from, next) => {

    var permissionValue = router.currentRoute.meta.viewScope;
    if(app.$auth.getPayload()){
        var allPermissions = app.$auth.getPayload().scopes;

        let user = JSON.parse(router.app.$store.getters.getAuthUser);

        if(user && !router.currentRoute.meta.requiresAuth){
            router.push({ name : 'conduct-incidents'});
        }

        if(user && !router.currentRoute.meta.requiresBoth && !router.currentRoute.meta.requiresAdmin && allAdmins.includes(user.role_id)){
            router.push({ name : 'conduct-incidents'});
        }

        if(user && router.currentRoute.meta.requiresAdmin && !allAdmins.includes(user.role_id)){
            router.push({ name : 'student-portal-list-view'});
        }


        if(allPermissions){ 
            if(!allPermissions.includes(permissionValue))
            {

                if(user && user.role_id == 5){
                    router.push({ name : 'student-portal-list-view'});
                }else{
                    router.push('/');
                }
            }
        }
    }
});


var permissionValue = router.currentRoute.meta.viewScope;
if(app.$auth.getPayload()){
    let user = JSON.parse(router.app.$store.getters.getAuthUser);

    if(user && !router.currentRoute.meta.requiresAuth){
        router.push({ name : 'conduct-incidents'});
    }

    if(user && !router.currentRoute.meta.requiresBoth && !router.currentRoute.meta.requiresAdmin && allAdmins.includes(user.role_id)){
        router.push({ name : 'conduct-incidents'});
    }

    if(user && router.currentRoute.meta.requiresAdmin && !allAdmins.includes(user.role_id)){
        router.push({ name : 'student-portal-list-view'});
    }


    
    var allPermissions = app.$auth.getPayload().scopes;
    if(allPermissions){       
        if(!allPermissions.includes(permissionValue))
        {
            console.log(permissionValue, ' not included hence redirecting');
            router.push('/');
        }
    }
}


if(!app.$auth.getPayload()){
    if(router.currentRoute.meta.requiresAuth){
     router.push('/');   
 }
}


export default router
