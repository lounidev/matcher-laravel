require('./bootstrap');
window.Vue = require('vue');
window.moment = require('moment');

// Local Registration in a Module System
import Vue from 'vue';
import VueRouter from 'vue-router';
import router from './routes';
import ShardsVue from 'shards-vue'
import AmCharts from 'amcharts3';
import AmSerial from 'amcharts3/amcharts/serial';
import AmPie from 'amcharts3/amcharts/pie';
import AmFunnelChart  from 'amcharts3/amcharts/funnel';
import AmGauge from 'amcharts3/amcharts/gauge';
import vbclass from 'vue-body-class';
var VueScrollTo = require('vue-scrollto');
import VueTimepicker from 'vue2-timepicker';
import Selectize from 'vue2-selectize';
import VueCodeHighlight from 'vue-code-highlight';
var vueDatepicker = require("vue-datepicker");
import Paginate from 'vuejs-paginate';
Vue.component('paginate', Paginate);
import StarRating from 'vue-star-rating';
import draggable from 'vuedraggable';
import Vuetify from 'vuetify';
import DaySpanVuetify from 'dayspan-vuetify';
import VeeValidate from 'vee-validate'
import InfiniteLoading from 'vue-infinite-loading'

import VueAxios from 'vue-axios';
import VueAuthenticate from 'vue-authenticate';
import axios from 'axios';
import Vuex from 'vuex';
import store from './store.js';
import wysiwyg from "vue-wysiwyg";
import html2canvas from 'html2canvas';
import jspdf from 'jspdf';
import VueMq from 'vue-mq';
 
Vue.use(VueMq, {
  breakpoints: { 
    mobile: 450,
    tablet: 900,
    laptop: 1250,
  }
});

Vue.use(html2canvas);
Vue.use(jspdf);

Vue.use(VueAxios, axios)

Vue.use(VueAuthenticate, {
    tokenName: 'admin_access_token',
    baseUrl: '/',
    loginUrl: '/api/auth/login',
    registerUrl: '/api/auth/register',
    logoutUrl: '/api/auth/logout',
    providers: {
        // Define OAuth providers config
        oauth2: {
            name: 'oauth2',
            url: 'Token/Exchange',
        }
    }
})

Vue.use(Vuex);

import Treeselect from '@riophae/vue-treeselect';
import VueProgressBar from 'vue-progressbar';
import VTooltip from 'v-tooltip';
import Multiselect from 'vue-multiselect';

window.googleReCaptchaSiteKey = googleReCaptchaSiteKey;

let veeCustomMessage = {
    en: {
        custom: {
            agree: {
                required: 'You must agree to the terms and conditions before registering!',
                digits: (field, params) => `length must be ${params[0]}`
            },
            privacypolicy: {
                required: 'You must agree the privacy policy before registering!',
                digits: (field, params) => `length must be ${params[0]}`
            },
            password_confirmation: {
                confirmed: 'Password does not match.'
            },
        }
    }
};
const config = {
    errorBagName: 'errorBag', // change if property conflicts.
    dictionary:  veeCustomMessage,
    events: 'input' 
};

// Use plugins by calling the Vue.use() global method
Vue.use(VueRouter);
Vue.use(router);
Vue.use(ShardsVue);
Vue.use( vbclass, router );
Vue.use(VueTimepicker);
Vue.use(Selectize);
Vue.use(VueCodeHighlight);
Vue.use(StarRating);
Vue.use(require('vue-faker'));
Vue.use(draggable);
Vue.use(Vuetify);
Vue.use(VeeValidate,config);
Vue.use(InfiniteLoading);
Vue.use(Treeselect);

Vue.use(VueProgressBar, options);
Vue.use(wysiwyg, {
    hideModules: { 
        "orderedList": true ,
        "unorderedList": true ,
        "image": true,
        "table": true ,
        "header": true,
        "iconPack" : 'icomoon' 
    },
});

// options to the toast
let options = {
    type : 'error',
    icon : 'error_outline',
    // ['success', 'info', 'error']
};


Vue.use(DaySpanVuetify, {
    methods: {
        getDefaultEventColor: () => '#1976d2'
    }
});
Vue.use(VueScrollTo, {
    container: "body",
    duration: 2000,
    easing: "ease",
    offset:-65,
    cancelable: true,
    onStart: false,
    onDone: false,
    onCancel: false,
    x: false,
    y: true
});
Vue.use(VTooltip);
Vue.use(Multiselect);
// const options = {
//     color: '#CF8D2E',
//     failedColor: '#8200ff',
//     thickness: '5px',
//     transition: {
//         speed: '2s',
//         opacity: '0.6s',
//         termination: 600
//     },
//     autoRevert: true,
//     location: 'top',
//     inverse: false
// }

require('./directives')



Vue.mixin({
   data: function() {
     return {
        globalReadOnlyProperty() {
            return  $route.name;
        }
    }
}
})

Vue.mixin({
  data: function() {
    return {
      globalVar:false,
  }
}
})

// Require components tags
require('./components-tags');
require('./filters')
//require('./directives')

require('./route-middleware')

const app = new Vue({
    el: '#app',
    router,
    store,
    mounted () {
        this.$Progress.finish();
    },
    created () {
        this.$Progress.start()
        this.$router.beforeEach((to, from, next) => {
            if (to.meta.progress !== undefined) {
                let meta = to.meta.progress
                this.$Progress.parseMeta(meta)
            }
            this.$Progress.start()
            next()
        })
        this.$router.afterEach((to, from) => {
            this.$Progress.finish()
            window.scrollTo(0,0);
        })

    },
    watch:{
        // '$route': function(from, to) {
        // }
    }
});

require('./interceptor')