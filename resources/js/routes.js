// Route components
import VueRouter from 'vue-router'


import CommonView from './components/common-components/CommonView.vue';


const routes = [

/*Login*/
{
    name: 'login',
    path: '/',
    component: require('./components/auth/Main.vue'),
    meta: {
        title: 'Login',
        bodyClass: 'login-page',
        noHeader: true,
    },
},

/*=== Conduct incidents ===*/
{
    name: 'conduct-incidents',
    path: '/conduct-incidents',
    meta: {
        title: 'Conduct Incidents',
        bodyClass: 'conduct-incident',
        requiresAuth: true,
        requiresAdmin : true,
        viewScope : 'conduct-incidents-section.view'
    },
    component: require('./components/conduct-incidents/dashboard/Main.vue'),
},

/*=== Conduct incidents  new case ===*/
{
    name: 'new-case',
    path: '/conduct-incidents/new',
    meta: {
        title: 'Conduct Incidents',
        bodyClass: 'conduct-incident case-new',
        pageTypeNewCase:true,
        requiresAuth: true,
    },
    component: require('./components/conduct-incidents/case-details/Main.vue'),
},

/*=== Conduct incidents  case details ===*/
{
    name: 'case-details',
    path: '/conduct-incidents/case-details',
    meta: {
        title: 'Case Details',
        bodyClass: 'new-ir',
        pageTypeCaseDetails:true,
        requiresAdmin : true,
        requiresAuth: true,
    },
    component: require('./components/conduct-incidents/case-details/Main.vue'),
},


{
    name: 'incident-case.view',
    path: '/conduct-incidents/case-details/:id',
    meta: {
        title: 'Case Details',
        bodyClass: 'new-ir',
        pageTypeCaseDetails:true,
        requiresAdmin : true,
        requiresAuth: true,
        viewScope : "incident-case.show"
    },
    component: require('./components/conduct-incidents/case-details/Main.vue'),
},

/*=== Conduct incidents Export Case PDF===*/
{
    name: 'export-case',
    path: '/pdf/:id',
    meta: {
        title: 'Export Case PDF',
        bodyClass: 'new-ir',
        noHeader: true,
        requiresAdmin : true,
        requiresAuth: false,
        viewScope : "incident-case.show"
    },
    component: require('./components/conduct-incidents/case-details/ExportCasePDF.vue'),
},
{
     name: 'export-report',
     path: '/reports/pdf/:type',
     component: require('./components/reports/export/ExportReportPDFMain.vue'),
     props: true,
     meta: {
         title: 'Current Situation',
         bodyClass: 'report-current-situation',
         noHeader: true,
         requiresAuth: false,
         exportReport: true,
         reportLink: true,
         requiresAdmin : true,
         viewScope : 'reports-section.view'

     },
 },

/* Students */
{
    name: 'students',
    path: '/students',
    meta: {
        title: 'Students',
        bodyClass: 'students',
        requiresAuth: true,
        requiresBoth : true,
        viewScope : 'student-section.view'        
    },
    component: require('./components/students/Main.vue'),
},

/* Employee */
{
    name: 'employee',
    path: '/employee',
    meta: {
        title: 'Employee',
        bodyClass: 'employee',
        requiresAuth: true,
        requiresAdmin : true,
        viewScope : 'employee-section.view'        
    },
    component: require('./components/employee/Main.vue'),
},

/* system listing  */
{
    name: 'system-log',
    path: '/system-log',
    meta: {
        title: 'system log',
        bodyClass: 'system-log',
        requiresAuth: true,
        requiresAdmin : true,
        viewScope : 'system-log-section.view'        

    },
    component: require('./components/system-log/Main.vue'),
},

/* Configurations */
{
    path: '/configurations',
    meta: {
        title: 'Configurations',
        bodyClass: 'configurations-body',
        requiresAuth: true,
        requiresAdmin : true,
    },
    component: require('./components/configurations/Main.vue'),
    children: [
    {
        name: 'ir-form-builder',
        path: 'ir-form-builder',
        component: require('./components/configurations/ir-form-builder/Main.vue'),
        meta: {
            title: 'Ir Form Builder',
            bodyClass: 'if-form-builder',
            requiresAuth: true,
            viewScope : 'ir-form-builder-section.view',
            requiresAdmin : true,

        },
    },
    {
        name: 'configuration-data-importer',
        path: 'data-importer',
        component: require('./components/configurations/DataImporter.vue'),
        meta: {
            title: 'Data Importer',
            bodyClass: 'configurations-data-importer',
            requiresAuth: true,
            viewScope : 'data-importer-section.view',
            requiresAdmin : true,

        },
    },
    {
        name: 'configuration-buildings',
        path: 'buildings',
        component: CommonView,
        meta: {
            componentName : 'building',
            apiRoute : 'location',
            params : {
                'is_building' : 1,
            },

            title: 'Configurations-buildings',
            bodyClass: 'configurations-buildings',
            requiresAuth: true,
            requiresAdmin : true,
            tableTitle: 'building',
            viewScope : 'buildings-section.view'
        },
    },
    {
        name: 'configuration-specific-locations',
        path: 'specific-locations',
        component: CommonView,
        meta: {
            componentName : 'specific-location',
            apiRoute : 'specific-location',
            params : {
                //'is_building' : 1,
            },

            title: 'Configurations-specific-locations',
            bodyClass: 'configurations-specific-locations',
            requiresAuth: true,
            requiresAdmin : true,
            tableTitle: 'specific-location',
            viewScope : 'specific-location-section.view'
        },
    }, 
    {
        name: 'configuration-plea-outcomes',
        path: 'plea-outcomes',
        component: CommonView,
        meta: {
            componentName : 'plea-outcomes',
            apiRoute : 'plea-outcome',
            params : {
                //'is_building' : 1,
            },

            title: 'Configurations-plea-outcomes',
            bodyClass: 'configurations-plea-outcomes',
            requiresAuth: true,
            requiresAdmin : true,
            tableTitle: 'plea-outcome',
            viewScope : 'plea-outcome-section.view'
        },
    }, 
    {
        name: 'configuration-locations',
        path: 'locations',
        component: CommonView,
        meta: {
            componentName : 'locations',
            apiRoute : 'location',
            params : {
                'is_building' : 0
            },
            title: 'Configurations-locations',
            bodyClass: 'configurations-locations',
            requiresAuth: true,
            requiresAdmin : true,

            tableTitle: 'location',
            viewScope : 'locations-section.view'
        },
    },
    {
        name: 'configuration-hearing-locations',
        path: 'hearing-locations',
        component: CommonView,
        meta: {
            componentName : 'hearing-locations',
            apiRoute : 'hearing-location',
            title: 'Configurations-hearing-locations',
            bodyClass: 'configurations-hearing-locations',
            requiresAuth: true,
            requiresAdmin : true,

            tableTitle: 'hearing-location',
            viewScope : 'hearing-types-section.view'

        },
    },
    {
        name: 'configuration-violations',
        path: 'violations',
        component: CommonView,
        meta: {
            componentName : 'violations',
            apiRoute : 'violation',
            title: 'Configurations-violations',
            bodyClass: 'configurations-violations',
            requiresAuth: true,
            requiresAdmin : true,

            tableTitle: 'violation',
            viewScope : 'violations-section.view'

        },
    },
    {
        name: 'configuration-plea-types',
        path: 'plea-types',
        component: CommonView,
        meta: {
            componentName : 'plea-types',
            apiRoute : 'plea-type',
            title: 'Configurations-plea-types',
            bodyClass: 'configurations-plea-types',
            requiresAuth: true,
            requiresAdmin : true,

            tableTitle: 'plea-type',
            viewScope : 'plea-types-section.view'

        },
    },
    {
        name: 'configuration-sanctions',
        path: 'sanctions',
        component: CommonView,
        meta: {
            componentName : 'sanctions',
            apiRoute : 'sanction',
            title: 'Configurations-sanctions',
            bodyClass: 'configurations-sanctions',
            requiresAuth: true,
            requiresAdmin : true,

            tableTitle: 'sanction',
            viewScope : 'sanctions-section.view'

        },
    },
    {
        name: 'configuration-fines',
        path: 'fines',
        component: CommonView,
        meta: {
            componentName : 'fines',
            apiRoute : 'fine',
            title: 'Configurations-fines',
            bodyClass: 'configurations-fines',
            requiresAuth: true,
            requiresAdmin : true,

            tableTitle: 'fine',
            viewScope : 'fines-section.view'

        },
    },    
    {
        name: 'configuration-title-ix-categories',
        path: 'title-ix-categories',
        component: CommonView,
        meta: {
            componentName : 'title-IX-categories',
            apiRoute : 'title-ix-category',
            title: 'Configurations-title-ix-categories',
            bodyClass: 'configurations-title-ix-categories',
            requiresAuth: true,
            requiresAdmin : true,

            tableTitle: 'title-IX-category',
            viewScope : 'title-ix-category-section.view'

        },
    },    


    {
        name: 'configuration-board-member-types',
        path: 'board-member-types',
        component: CommonView,
        meta: {
            componentName : 'board-types',
            apiRoute : 'board-member-type',
            title: 'Configurations-board-member-types',
            bodyClass: 'configurations-board-member-types',
            requiresAuth: true,
            requiresAdmin : true,

            tableTitle: 'board-type',
            viewScope : 'board-members-section.view'

        },
    },

    {
        name: 'configuration-appeal-outcomes',
        path: 'appeal-outcomes',
        component: CommonView,
        meta: {
            componentName : 'appeal-outcome',
            apiRoute : 'appeal-outcome',
            title: 'Configurations-appeal-outcomes',
            bodyClass: 'configurations-appeal-outcomes',
            requiresAuth: true,
            requiresAdmin : true,

            tableTitle: 'appeal-outcome',
            viewScope : 'appeal-outcomes-section.view'

        },
    },    {
        name: 'configuration-appeal-types',
        path: 'appeal-types',
        component: CommonView,
        meta: {
            componentName : 'appeal-types',
            apiRoute : 'appeal-type',
            title: 'Configurations-appeal-types',
            bodyClass: 'configurations-appeal-types',
            requiresAuth: true,
            requiresAdmin : true,

            tableTitle: 'appeal-type',
            viewScope : 'appeal-types-section.view'

        },
    },
    {
        name: 'configuration-appeal-statuses',
        path: 'appeal-statuses',
        component: CommonView,
        meta: {
            componentName : 'appeal-status',
            apiRoute : 'appeal-status',
            title: 'Configurations-appeal-statuses',
            bodyClass: 'configurations-appeal-statuses',
            requiresAuth: true,
            requiresAdmin : true,

            tableTitle: 'appeal-status',
            viewScope : 'appeal-status-section.view'

        },
    }, 
    {
        name: 'configuration-incident-type',
        path: '/',
        component: require('./components/configurations/IncidentTypes.vue'),
        meta: {
            title: 'Configurations-Incident-type',
            bodyClass: 'configurations-incident-type',
            viewScope : 'incident-type-section.view',
            requiresAdmin : true,
            requiresAuth: true,
            

        },
    },
    {
        name: 'configuration-incident-type',
        path: 'incident-type',
        component: require('./components/configurations/IncidentTypes.vue'),
        meta: {
            title: 'Configurations-Incident-type',
            bodyClass: 'configurations-incident-type',
            viewScope : 'incident-type-section.view',
            requiresAdmin : true,
            requiresAuth: true,
            

        },
    },
    {

        name: 'configuration-workflows',
        path: 'workflows',
        component: require('./components/configurations/work-flows/Main.vue'),
        meta: {
            title: 'Configurations-workflows',
            bodyClass: 'configurations-workflows',
            requiresAuth: true,
            viewScope : 'configurations-section.view',
            requiresAdmin : true,


        },
    },
    {

        name: 'configuration-checklist',
        path: 'checklist',
        component: require('./components/configurations/checklist/Main.vue'),
        meta: {
            title: 'Configurations-checklist',
            bodyClass: 'configurations-checklist',
            requiresAuth: true,
            viewScope : 'checklist-section.view',
            requiresAdmin : true,


        },
    },
    {

        name: 'configuration-email-templates',
        path: 'email-templates',
        component: require('./components/configurations/email-template/Main.vue'),
        meta: {
            title: 'Configurations-email-templates',
            bodyClass: 'configurations-email-templates',
            requiresAuth: true,
            viewScope : 'email-template-section.view',
            requiresAdmin : true,


        },
    },
    {
        name: 'configuration-appeal-reviews',
        path: 'appeal-reviews',
        component: CommonView,
        meta: {
            componentName : 'appeal-review',
            apiRoute : 'appeal-review-type',
            title: 'Configurations-appeal-reviews',
            bodyClass: 'configurations-appeal-reviews',
            requiresAuth: true,
            tableTitle: 'appeal-review',
            viewScope : 'appeal-reviews-section.view',
            requiresAdmin : true,


        },
    },
    {
        name: 'configuration-hearing-types',
        path: 'hearing-types',
        component: CommonView,
        meta: {
            componentName : 'hearing-types',
            apiRoute : 'hearing-type',
            title: 'Configurations-hearing-types',
            bodyClass: 'configurations-hearing-types',
            requiresAuth: true,
            tableTitle: 'hearing-type',
            viewScope : 'hearing-types-section.view',
            requiresAdmin : true,


        },
    },
    {
        name: 'configuration-student-flags',
        path: 'student-flags',
        component: CommonView,
        meta: {
            componentName : 'flag-status',
            apiRoute : 'student-flag',
            title: 'Configurations-student-flags',
            bodyClass: 'configurations-student-flags',
            requiresAuth: true,
            tableTitle: 'flag-status',
            viewScope : 'flag-status-section.view',
            requiresAdmin : true,


        },
    },
    {
        name: 'configuration-incident-role',
        path: 'incident-roles',
        component: CommonView,
        meta: {
            componentName : 'incident-roles',
            apiRoute : 'involved-user-type',
            title: 'Configurations-incident-roles',
            bodyClass: 'configurations-incident-roles',
            requiresAuth: true,
            tableTitle: 'incident-role',
            viewScope : 'involved-user-type-section.view',
            requiresAdmin : true,
        },
    },
    {
        name: 'configuration-display-settings',
        path: 'display-settings',
        component: require('./components/configurations/DisplaySettings.vue'),
        meta: {
            title: 'Display Settings',
            bodyClass: 'configurations-display-setting',
            requiresAuth: true,
            viewScope : 'display-settings-section.view',
            requiresAdmin : true,


        },
    },
    {
        name: 'configuration-organization',
        path: 'organization',
        component: require('./components/configurations/Organization.vue'),
        meta: {
            title: 'Configurations-Incident-type',
            bodyClass: 'configurations-incident-type',
            viewScope : 'organization.view',
            requiresAdmin : true,
            requiresAuth: true,
            

        },
    }
    ],
},



/* Student Portal Conduct Incident */


{
    name: 'student-portal-list-view',
    path: '/student-portal',
    meta: {
        title: 'My Cases',
        bodyClass: 'student_portal_conduct_incident',
        currentPortal: 'student',
        requiresAuth: true,
        viewScope : 'student-conduct-incidents-section.view',
    },
    component: require('./components/student-portal/case-listing/Main.vue'),
},
{
    name: 'student.case.detail',
    path: '/student-portal/case-details/:id',
    meta: {
        title: 'Case No',
        bodyClass: 'student_portal_conduct_incident_detail',
        currentPortal: 'student',
        requiresAuth: true,
        viewScope : 'student-conduct-incidents-section.view',
    },
    component: require('./components/student-portal/case-detail/Main.vue'),
},
{
    name: 'student-portal-new-report',
    path: '/student-portal/new-report',
    meta: {
        title: 'New Report',
        bodyClass: 'student_portal_new_report incident-reporting incident-reporting-employee',
        currentPortal: 'student',
        requiresAuth: true,
        viewScope : 'incident-report-section.view'
    },
    // component: require('./components/student-portal/new-case/Main.vue'),
    component: require('./components/incident-reporting/Main.vue'),
},


     // Reports 
     {
         path: '/reports',
         meta: {
             title: 'Reports',
             bodyClass: 'report-body',
             requiresAuth : true,
             requiresAdmin : true,
             viewScope : 'reports-section.view'

         },
         component: require('./components/reports/Main.vue'),
         children: [
         {
             name: 'report-overall-statistics',
             path: '/',
             component: require('./components/reports/overall-statistics/Main.vue'),
             meta: {
                 title: 'Overall Statistics',
                 bodyClass: 'report-overall-statistics',
                 exportReport: true,
                 reportLink: true,
                 requiresAuth : true,
                 requiresAdmin : true,
                 viewScope : 'reports-section.view'
             },
         },
         {
             name: 'report-current-situation',
             path: '/reports/current-situation',
             component: require('./components/reports/current-situation/Main.vue'),
             meta: {
                 title: 'Current Situation',
                 bodyClass: 'report-current-situation',
                 exportReport: true,
                 reportLink: true,
                 requiresAuth : true,
                 requiresAdmin : true,
                 viewScope : 'reports-section.view'

             },
         },

         {
             name: 'custom-report-builder',
             path: '/reports/custom-report-builder/:id',
             component: require('./components/reports/custom-report-builder/Main.vue'),
             props: true,
             meta: {
                 title: 'Custom Reports Builder',
                 bodyClass: 'custom-report-builder',
                 reportLink: true,
                 requiresAuth : true,
                 requiresAdmin : true,
                 viewScope : 'reports-section.view'

             },
         },
         {
             name: 'custom-report-builder',
             path: '/reports/custom-report-builder/',
             component: require('./components/reports/custom-report-builder/Main.vue'),
             props: true,
             meta: {
                 title: 'Custom Reports Builder',
                 bodyClass: 'custom-report-builder',
                 reportLink: true,
                 requiresAuth : true,
                 requiresAdmin : true,
                 viewScope : 'reports-section.view'

             },
         },

         {
             name: 'saved-reports',
             path: '/reports/saved-reports',
             component: require('./components/reports/saved-reports/Main.vue'),
             meta: {
                 title: 'Saved Reports',
                 bodyClass: 'report-saved-reports',
                 reportLink: true,
                 requiresAuth : true,
                 requiresAdmin : true,
                 viewScope : 'reports-section.view'

             },
         },

         {
             name: 'scheduled-reports',
             path: '/reports/scheduled-reports',
             component: require('./components/reports/scheduled-reports/Main.vue'),
             meta: {
                 title: 'Scheduled Reports',
                 bodyClass: 'report-scheduled-reports',
                 reportLink: true,
                 requiresAuth : true,
                 requiresAdmin : true,
                 viewScope : 'reports-section.view'

             },
         },

         {
             name: 'generated-reports',
             path: '/reports/generated-reports',
             component: require('./components/reports/generated-reports/Main.vue'),
             meta: {
                 title: 'Generated Reports',
                 bodyClass: 'report-generated-reports',
                 reportLink: true,
                 requiresAuth : true,
                 viewScope : 'reports-section.view',
                 requiresAdmin : true,

             },
         },

         {
             path: '/custom-report-generated/:id',
             name : 'custom-report-generated.view',
             meta: {
                 params:{
                    is_saved: false
                 },
                 title: 'Reports',
                 reportLink: true,
                 exportReport: true,
                 showReportSchedule: true,
                 bodyClass: 'report-body',
                 requiresAuth : true,
                 requiresAdmin : true,
                 viewScope : 'reports-section.view',
                 downloadCsv: true
             },
             component: require('./components/reports/generated-reports/Main.vue'),
         },
         {
             path: '/custom-report/:id',
             name : 'custom-report.view',
             meta: {
                 params:{
                     is_saved: true,
                 },
                 title: 'Reports',
                 bodyClass: 'report-body',
                 requiresAuth : true,
                 requiresAdmin : true,
                 viewScope : 'reports-section.view'

             },
             component: require('./components/reports/generated-reports/Main.vue'),
         },

         
         ]
     },
     /* Incident Reporting */
     {
         name: 'incident-reporting-employee',
         path: '/incident-reporting/new',
         meta: {
             title: 'Incident Reporting Employee',
             bodyClass: 'incident-reporting incident-reporting-employee',
             requiresAuth: true,
             requiresAdmin : true,
             viewScope : 'reports-section.view'

         },
         component: require('./components/incident-reporting/Main.vue'),
     },
     /* Incident Report Update */
     {
         name: 'incident-reporting-employee.update',
         path: '/incident-reporting/:id',
         props: true,
         meta: {
             title: 'Incident Reporting Employee',
             bodyClass: 'incident-reporting incident-reporting-employee',
             requiresAuth: true,
             requiresAdmin : true,
             viewScope : 'reports-section.view'
         },
         component: require('./components/incident-reporting/Main.vue'),
     },


     {
         name: 'student-portal-new-report.update',
         path: '/student-portal/new-report/:id',
         props: true,
         meta: {
             title: 'Incident Report',
             bodyClass: 'student_portal_new_report incident-reporting incident-reporting-employee',
             currentPortal: 'student',
             requiresAuth: true,
             viewScope : 'incident-report-section.view'
         },
    // component: require('./components/student-portal/new-case/Main.vue'),
    component: require('./components/incident-reporting/Main.vue'),
},


{
    name: 'student-portal-new-report',
    path: '/student-portal/new-report',
    meta: {
        title: 'New Report',
        bodyClass: 'student_portal_new_report incident-reporting incident-reporting-employee',
        currentPortal: 'student',
        requiresAuth: true,
        viewScope : 'incident-report-section.view'
    },
    // component: require('./components/student-portal/new-case/Main.vue'),
    component: require('./components/incident-reporting/Main.vue'),
},
/* Guest Incident Reporting */
{
    name: 'incident-reporting-guest',
    path: '/incident-reporting',
    meta: {
        title: 'Incident Reporting Guest',
        bodyClass: 'incident-reporting',
        noHeader: true,
        requiresAuth: false,
        requiresAdmin : false,
        viewScope : 'reports-section.view'
    },
    component: require('./components/incident-reporting/GuestMain.vue'),
},
/* My Cases */
{
    name: 'my-cases-list-view',
    path: '/my-cases',
    meta: {
        title: 'My Cases',
        bodyClass: 'student_portal_conduct_incident',
        currentPortal: 'student',
        requiresAuth: true,
        requiresAdmin: true,
        requiresBoth: true,
        name: 'my.cases.detail',
        hideNewIncidentReport: true,
        viewScope : 'my-cases-section.view',
    },
    component: require('./components/student-portal/case-listing/Main.vue'),
},
{
    name: 'my.cases.detail',
    path: '/my-cases/case-details/:id',
    meta: {
        title: 'Case No',
        bodyClass: 'student_portal_conduct_incident_detail',
        currentPortal: 'student',
        requiresAuth: true,
        requiresBoth: true,
        requiresAdmin: true,
        modifyDetail: 'my-cases-new-report.update',
        viewScope : 'my-cases-section.view',
    },
    component: require('./components/student-portal/case-detail/Main.vue'),
},
{
    name: 'my-cases-new-report.update',
    path: '/my-cases/new-report/:id',
    props: true,
    meta: {
        title: 'Incident Report',
        bodyClass: 'student_portal_new_report incident-reporting incident-reporting-employee',
        currentPortal: 'student',
        requiresAuth: true,
        requiresBoth: true,
        requiresAdmin: true,
        viewScope : 'incident-report-section.view'
    },
    // component: require('./components/student-portal/new-case/Main.vue'),
    component: require('./components/incident-reporting/Main.vue'),
},
/*404 Page*/

{
    name: '404',
    path: '*',
    component: require('./components/404/Main.vue'),
    meta: {
        title: '404 Not Found',            
        bodyClass: 'not-found-page',
        noHeader: true,
    },
}]
// Create the router instance
const router = new VueRouter({
    mode: 'history',
    routes // short for `routes: routes`
});

export default router

