/*
|--------------------------------------------------------------------------
| Store > Auth > Actions
|--------------------------------------------------------------------------
|
| This file contains the actions property of Auth Vuex Module
|
| You may freely extend this store file if the store file that you will
| be building has similar characteristics.
|
*/
'use strict';

export default {

    /**
     * Update the Two Factor Auth Enabler / Disabler
     *
     * @param  {object} context
     *   : the context of $store
     *
     * @param  {object} payload
     *   : an object which contains option values
     *
     * @return {*}
     */
    update2FA: function (context, payload) {}, // End of update2FA

} // End of export default