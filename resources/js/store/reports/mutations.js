/*
|--------------------------------------------------------------------------
| Store > Auth > Mutations
|--------------------------------------------------------------------------
|
| This file contains the mutations property of Auth Vuex Module
|
| You may freely extend this store file if the store file that you will
| be building has similar characteristics.
|
*/
'use strict';

export default {


    /**
     * Set the userData
     *
     * @param  {object} state
     *   : the state property the current Vuex Object
     *
     * @param  {object} userData
     *   : an object which contains the user's data
     *
     * @return {void}
     */
    setUserData: function (state, userData) {}, // End of setUserData method

} // End of export default