/*
|--------------------------------------------------------------------------
| Store > Auth > Getters
|--------------------------------------------------------------------------
|
| This file contains the getters property of Auth Vuex Module
|
| You may freely extend this store file if the store file that you will
| be building has similar characteristics.
|
*/
'use strict';

export default {

    /**
     * Get the User Roles
     *
     * @param  {object} state
     *   : the current state of the current Vuex Object
     *
     * @return {object}
     */
    getColors: function (state) {
        return state.colorCodes;
    },

    getMonthNames: function (state) {
        return state.monthNames;
    },

} // End of export default

