/*
|--------------------------------------------------------------------------
| Store > Auth > State
|--------------------------------------------------------------------------
|
| This file contains the state property of Auth Vuex Module
|
| You may freely extend this store file if the store file that you will
| be building has similar characteristics.
|
*/
'use strict';

export default {

    /**
     * Value for the access token for API consumption.
     * @type {string|null}
     */
     colorCodes : [
     "#8FD0E1",
     "#C0DA6D",
     "#A5B3F4",
     "#C570C8",
     "#E2B566",
     "#C8BAAC",
     "#F9CFF2",
     "#DAE0F2",
     "#C4E4A4",
     "#E7E7CB",
     "#CFB693",
     "#F0F3BD",
     "#EFC7C2",
     "#FFB4A2",
     "#B8BEDD",
     "#FFCAD4",
     "#D8E2DC", 
     "#9CAFB7",
     "#CDEAC0",
     "#EDEEC9", 
     "#FFA5AB",
     "#D7B9D5", 
     "#C4FFF9", 
     "#BFD8BD",
     "#BAF2BF",
     "#EFC3E6",
     "#94D0C7",
     "#FF9999",
     "#7AE7C7",
     "#E5B25D",
     "#CACAAA",
     "#F3B3A6",
     "#e49977",
     "#38618C",
     "#c0da6c",
     "#FF5964",
     "#5CB85C",
     "#C2E7D9",
     "#F49CBB",
     "#DAEFDE",
     "#afe5f9",
     "#c56fc8",
     "#FADF63",
     "#91C2FF",
     "#8fd1e1",
     "#7E8D85",
     "#e49977",
     "#FAC9B8",
     "#F5CB5C",
     "#FC6759",
     "#E6E4CE",
     "#2EC4B6",
     "#FFA69E",
     "#8D99AE",
     "#BEE3DB",
     "#FFD6BA",
     "#F79D65",
     "#84DCC6",
     "#73CDE4",
     "#BCE784",
     "#5DD39E",
     ],


     monthNames : {
         "1" : 'Jan',
         "2" : 'Feb',
         "3" : 'Mar',
         "4" : 'Apr',
         "5" : 'May',
         "6" : 'June',
         "7" : 'Jul',
         "8" : 'Aug',
         "9" : 'Sept',
         "10" : 'Oct',
         "11" : 'Nov',
         "12" : 'Dec',
     },



} // End of export default