//--General Components--//
//header
Vue.component('c-header', require('./components/common-components/ComHeader.vue'));
//main navigation
Vue.component('c-navigation', require('./components/common-components/ComNavigation.vue'));
//Left Navigation
Vue.component('left-nav', require('./components/common-components/LeftSideNav.vue'));
//notification block
Vue.component('c-notification', require('./components/common-components/ComNotification.vue'));
//logo
Vue.component('c-logo', require('./components/common-components/ComLogo.vue'));
//overlay
Vue.component('c-overlay', require('./components/common-components/ComOverlay.vue'));
// User Profile
Vue.component('user-profile', require('./components/common-components/UserProfile.vue'));
Vue.component('user-profile-dropdown', require('./components/common-components/UserProfileDropDown.vue'));

// Student Portal Left Nav
Vue.component('left-nav-student', require('./components/common-components/LeftSideNavStudent.vue'));

//notification pagination
Vue.component('c-pagination', require('./components/common-components/ComPagination.vue'));
//sidebar style 2
Vue.component('left-nav-config', require('./components/common-components/LeftSideNavConfig.vue'));
//loader
Vue.component('c-loader', require('./components/common-components/ComLoader.vue'));
//alert
// Vue.component('c-alert', require('./components/common-components/ComAlert.vue'));
//No Record
Vue.component('no-record-found', require('./components/common-components/NoRecord.vue'));
//Info Message
Vue.component('info-message', require('./components/common-components/InfoMessage.vue'));
// Block Spinner
Vue.component('block-spinner', require('./components/common-components/BlockSpinner.vue'));

//notification block-2', require('./components/common-components/SideBar2.vue'));

//--Auth Components--//
// Login
Vue.component('c-login', require('./components/auth/AuthLogin.vue'));
//forgot
Vue.component('c-forgot', require('./components/auth/AuthForgot.vue'));
//check your inbox
Vue.component('c-check-inbox', require('./components/auth/AuthCheckInbox.vue'));
// Auth Footer
Vue.component('auth-footer', require('./components/auth/AuthFooter.vue'));

//--conduct-incidents--//
//workflow
Vue.component('work-flow', require('./components/conduct-incidents/dashboard/WorkFlow.vue'));
Vue.component('work-flow-component', require('./components/conduct-incidents/dashboard/WorkflowComponent.vue'));
//Listing
Vue.component('incident-listing', require('./components/conduct-incidents/dashboard/Listing.vue'));
Vue.component('case-detail', require('./components/conduct-incidents/case-details/CaseDetail.vue'));
Vue.component('other-details', require('./components/conduct-incidents/case-details/OtherDetails.vue'));
Vue.component('related-cases', require('./components/conduct-incidents/case-details/RelatedCases.vue'));
Vue.component('case-title-ix', require('./components/conduct-incidents/case-details/TitleIX.vue'));
Vue.component('clery-act-details', require('./components/conduct-incidents/case-details/CleryActDetails.vue'));
//conduct-incidents-detail
Vue.component('detail-sidebar', require('./components/conduct-incidents/case-details/DetailSideBar.vue'));
Vue.component('case-uploads', require('./components/conduct-incidents/case-details/case-uploads/CaseUpload.vue'));

//case-uploads
Vue.component('schedule-meeting', require('./components/conduct-incidents/case-details/case-uploads/ScheduleMeeting.vue'));
Vue.component('send-email', require('./components/conduct-incidents/case-details/case-uploads/SendEmail.vue'));
Vue.component('notes', require('./components/conduct-incidents/case-details/case-uploads/Notes.vue'));
Vue.component('attach-files', require('./components/conduct-incidents/case-details/case-uploads/AttachFiles.vue'));
Vue.component('progress-bar', require('./components/conduct-incidents/case-details/case-uploads/ProgressBar.vue'));


Vue.component('case-activities', require('./components/conduct-incidents/case-details/CaseActivities.vue'));
Vue.component('class-schedule', require('./components/common-components/ClassSchedule.vue'));
// Activities conponents
// Activities tabs
Vue.component('activity-tabs', require('./components/conduct-incidents/case-details/ActivityTabs.vue'));
// All Activities

Vue.component('meeting-activity', require('./components/conduct-incidents/case-details/activity-listings/MeetingActivity.vue'));

Vue.component('html-components', require('./components/conduct-incidents/case-details/activity-listings/HtmlComponents.vue'));
// Case Detail Activities
Vue.component('case-detail-activity', require('./components/conduct-incidents/case-details/activity-listings/CaseDetails.vue'));
//attached documents
Vue.component('attachment-activity', require('./components/conduct-incidents/case-details/activity-listings/AttachmentActivity.vue'));
Vue.component('attached-documents', require('./components/conduct-incidents/case-details/activity-listings/AttachedDocuments.vue'));
//meeting schedule
Vue.component('meeting-schedule', require('./components/conduct-incidents/case-details/activity-listings/MeetingsSchedule.vue'));
//Notes
Vue.component('c-notes', require('./components/conduct-incidents/case-details/activity-listings/Notes.vue'));
//Sanction
Vue.component('sanction-activity', require('./components/conduct-incidents/case-details/activity-listings/Sanction.vue'));
//Appeal
Vue.component('appeal-activity', require('./components/conduct-incidents/case-details/activity-listings/Appeal.vue'));
//Log
Vue.component('logs-activity', require('./components/conduct-incidents/case-details/activity-listings/Logs.vue'));
//Email
Vue.component('email-activity', require('./components/conduct-incidents/case-details/activity-listings/ActivityEmails.vue'));
//CheckList
Vue.component('checklist-activity', require('./components/conduct-incidents/case-details/activity-listings/CheckList.vue'));
//Charges
Vue.component('charges-activity', require('./components/conduct-incidents/case-details/activity-listings/Charges.vue'));
//Plea
Vue.component('plea-activity', require('./components/conduct-incidents/case-details/activity-listings/Plea.vue'));
// students
Vue.component('student-listing', require('./components/students/StudentListing.vue'));
// Employee
Vue.component('employee-listing', require('./components/employee/EmployeeListing.vue'));
// system log
Vue.component('system-listing', require('./components/system-log/SystemListing.vue'));

//Configurations
Vue.component('config-incident-types', require('./components/configurations/IncidentTypes.vue'));


Vue.component('config-building', require('./components/configurations/Building.vue'));

Vue.component('edit-stage-popup', require('./components/configurations/work-flows/EditStagePopup.vue'));

// IR Form Builder
Vue.component('ir-form', require('./components/configurations/ir-form-builder/IrFormBuilder.vue'));
Vue.component('ir-right-tabs', require('./components/configurations/ir-form-builder/IrRightTabs.vue'));
Vue.component('ir-form-top-header', require('./components/configurations/ir-form-builder/TopHeader.vue'));
Vue.component('ir-form-add-component', require('./components/configurations/ir-form-builder/AddComponent.vue'));
Vue.component('ir-form-input-type', require('./components/configurations/ir-form-builder/InputType.vue'));

// Configuration Workflows
Vue.component('work-flow-listing', require('./components/configurations/work-flows/WorkFlowListing.vue'));
Vue.component('config-work-flow', require('./components/configurations/work-flows/ConfigWorkFlow.vue'));

// Configuration Email Template
Vue.component('email-listing', require('./components/configurations/email-template/EmailListing.vue'));
Vue.component('email-form', require('./components/configurations/email-template/EmailForm.vue'));
// Vue.component('involved-parties', require('./components/popups/ChangeCaseOwner.vue'));

// Popups

Vue.component('involved-parties', require('./components/conduct-incidents/case-details/InvolvedParties.vue'));
Vue.component('merge-case-popup', require('./components/popups/MergeCase.vue'));
Vue.component('split-case-popup', require('./components/popups/SplitCase.vue'));
Vue.component('discard-case-popup', require('./components/popups/DiscardCase.vue'));
Vue.component('reopen-case-popup', require('./components/popups/ReopenCase.vue'));
Vue.component('export-case-popup', require('./components/popups/ExportCase.vue'));
Vue.component('change-case-owner-popup', require('./components/popups/ChangeCaseOwner.vue'));
Vue.component('expected-date-popup', require('./components/popups/ExpectedDate.vue'));
Vue.component('dissociate-party-popup', require('./components/popups/DissociateParty.vue'));
Vue.component('mark-flag-popup', require('./components/popups/MarkFlag.vue'));
Vue.component('expected-date-popup', require('./components/popups/ExpectedDate.vue'));
Vue.component('associate-party-popup', require('./components/popups/AssociateParty.vue'));
Vue.component('case-risk-popup', require('./components/popups/CaseRisk.vue'));
Vue.component('add-related-case-popup', require('./components/popups/AddRelatedCase.vue'));
Vue.component('add-title-ix-popup', require('./components/popups/AddTitleIx.vue'));
Vue.component('remove-related-case-popup', require('./components/popups/RemoveRelatedCase.vue'));
Vue.component('remove-title-ix-popup', require('./components/popups/RemoveTitleIx.vue'));
Vue.component('change-clery-popup', require('./components/popups/ChangeClery.vue'));
Vue.component('change-other-detail-popup', require('./components/popups/ChangeOtherDetail.vue'));
Vue.component('clery-act-detail-popup', require('./components/popups/CleryActDetails.vue'));
Vue.component('manage-custom-field-popup', require('./components/popups/ManageCustomField.vue'));
Vue.component('case-detail-manage-custom-field-popup', require('./components/popups/CaseDetailManageCustomField.vue'));
Vue.component('clery-act-manage-custom-field-popup', require('./components/popups/CleryActManageCustomField.vue'));
Vue.component('change-status-popup', require('./components/popups/ChangeStatus.vue'));
Vue.component('manage-charges-popup', require('./components/popups/ManageCharges.vue'));
Vue.component('close-case-popup', require('./components/popups/CloseCase.vue'));
Vue.component('convert-case-popup', require('./components/popups/ConvertCase.vue'));
Vue.component('confirm-customfield-remove-popup', require('./components/popups/ConfirmCustomFieldRemovePopup.vue'));
Vue.component('manage-sanctions-popup', require('./components/popups/ManageSanctions.vue'));
Vue.component('manage-case-plea-popup', require('./components/popups/ManageCasePlea.vue'));
Vue.component('manage-appeals-popup', require('./components/popups/ManageAppeals.vue'));
Vue.component('student-details-popup', require('./components/popups/StudentDetails.vue'));
Vue.component('update-profile-popup', require('./components/popups/UpdateProfile.vue'));
Vue.component('contact-support-popup', require('./components/popups/ContactSupport.vue'));
Vue.component('role-popup', require('./components/popups/Role.vue'));
Vue.component('add-incident-popup', require('./components/popups/AddIncident.vue'));
Vue.component('add-building-popup', require('./components/popups/AddBuilding.vue'));
Vue.component('add-specific-location-popup', require('./components/popups/AddSpecificLocation.vue'));
Vue.component('add-locations-popup', require('./components/popups/AddLocations.vue'));
Vue.component('define-rule-popup', require('./components/popups/DefineRule.vue'));
Vue.component('add-violations-popup', require('./components/popups/AddViolations.vue'));
Vue.component('employee-details-popup', require('./components/popups/EmployeeDetails.vue'));
Vue.component('add-plea-type-popup', require('./components/popups/AddPleaType.vue'));
Vue.component('add-sanctions-popup', require('./components/popups/AddSanctions.vue'));
Vue.component('add-fines-popup', require('./components/popups/AddFines.vue'));
Vue.component('add-categories-popup', require('./components/popups/AddCategories.vue'));
Vue.component('add-appeal-types-popup', require('./components/popups/AddAppealTypes.vue'));
Vue.component('add-appeal-status-popup', require('./components/popups/AddAppealStatus.vue'));
Vue.component('add-appeal-review-popup', require('./components/popups/AddAppealReview.vue'));
Vue.component('add-appeal-outcome-popup', require('./components/popups/AddAppealOutcome.vue'));
Vue.component('add-hearing-types-popup', require('./components/popups/AddHearingTypes.vue'));
Vue.component('add-hearing-locations-popup', require('./components/popups/AddHearingLocations.vue'));
Vue.component('save-report-popup', require('./components/popups/SaveReport.vue'));
Vue.component('schedule-report-popup', require('./components/popups/ScheduleReport.vue'));
Vue.component('anonymous-user-popup', require('./components/popups/AnonymousUser.vue'));
Vue.component('add-flag-status-popup', require('./components/popups/AddFlagStatus.vue'));
Vue.component('delete-popup', require('./components/popups/DeleteItem.vue'));
Vue.component('delete-ir-popup', require('./components/popups/DeleteIrFormPopup.vue'));
Vue.component('delete-ir-rule-popup', require('./components/popups/DeleteIrRulePopup.vue'));
Vue.component('manage-clery-act-popup', require('./components/popups/ManageCleryAct.vue'));
Vue.component('view-report-listing-popup', require('./components/popups/ViewReportListing.vue'));
Vue.component('case-move-popup', require('./components/popups/CaseMove.vue'));
Vue.component('edit-checklist-popup', require('./components/popups/EditChecklist.vue'));
Vue.component('add-board-members-popup', require('./components/popups/AddBoardMembers.vue'));
Vue.component('contact-us-popup', require('./components/popups/ContactUs.vue'));
// Vue.component('cleart-act-info-popup', require('./components/popups/ViewClearAct.vue'));
Vue.component('add-board-members', require('./components/popups/AddMembers.vue'));
Vue.component('email-preview-popup', require('./components/popups/EmailPreview.vue'));
Vue.component('student-sanctions-popup', require('./components/popups/StudentPortalSanction.vue'));
Vue.component('student-appeals-popup', require('./components/popups/StudentAppeal.vue'));
Vue.component('add-organization-popup', require('./components/popups/AddOrganizationPopup.vue'));
Vue.component('organization-profile-popup', require('./components/popups/OrganizationProfilePopup.vue'));

Vue.component('report-info-popup', require('./components/popups/SaveReportAlert.vue'));
Vue.component('user-convert-popup', require('./components/popups/UserConvertAlertPopup.vue'));
// Incident Reporting Employee
Vue.component('incident-reporting-employee', require('./components/incident-reporting-employee/IncidentReportingEmployee.vue'));
Vue.component('incident-report-employee', require('./components/incident-reporting-employee/IncidentReportEmployee.vue'));
Vue.component('edit-workflow-popup', require('./components/popups/editWorkflowPopup.vue'));
Vue.component('view-special-code-popup', require('./components/popups/ViewSpecialCode.vue'));


// Modify Incident Reporting
Vue.component('modify-incident-reporting', require('./components/modify-incident-reporting/ModifyIncidentReporting.vue'));
Vue.component('modify-incident-report', require('./components/modify-incident-reporting/ModifyIncidentReport.vue'));

// Student Portal
Vue.component('student-portal-sidebar', require('./components/student-portal/case-detail/DetailSideBar.vue'));
Vue.component('student-portal-case-thumb', require('./components/student-portal/case-listing/Thumbnail.vue'));
Vue.component('student-activity-tabs', require('./components/student-portal/case-detail/ActivityTabs.vue'));
Vue.component('student-incident-report', require('./components/student-portal/new-case/IncidentReportingStudent.vue'));
Vue.component('student-report-submission', require('./components/student-portal/new-case/ReportSubmission.vue'));

//  Common Components and Methods

Vue.component('common-add-basic-popup', require('./components/common-components/AddBasicCommonPopup.vue'));
Vue.component('file-upload-component', require('./components/common-components/FileUpload.vue'));
Vue.component('common-methods', require('./components/common-components/CommonMethods.vue'));
Vue.component('common-listing', require('./components/common-components/CommonListing.vue'));
Vue.component('common-view', require('./components/common-components/CommonView.vue'));
Vue.component('common-alert-popup', require('./components/common-components/CommonAlertPopup.vue'));
Vue.component('custom-field-component', require('./components/common-components/CustomFieldComponent.vue'));
Vue.component('content-placeholder', require('./components/common-components/ContentPlaceholder.vue'));

// IR form
Vue.component('ir-form-component', require('./components/common-components/IRForm.vue'));
Vue.component('ir-form-component-update', require('./components/common-components/IRFormUpdate.vue'));


//404 components
Vue.component('not-found-panel', require('./components/404/NotFound.vue'));

//Report Components
Vue.component('report-navigation', require('./components/reports/ReportNaviagation.vue'));
Vue.component('report-panel', require('./components/reports/ReportPanel.vue'));
Vue.component('generated-report', require('./components/reports/custom-report-builder/GeneratedReport.vue'));
Vue.component('custom-report-builder', require('./components/reports/custom-report-builder/CustomReportBuilder.vue'));


//Current Situation
Vue.component('general-statistics', require('./components/reports/current-situation/GeneralStatistics.vue'));
Vue.component('incident-type-statistics', require('./components/reports/current-situation/IncidentTypeStatistics.vue'));
Vue.component('case-stage-statistics', require('./components/reports/current-situation/CaseStageStatistics.vue'));

// checklist Listing
Vue.component('checklist', require('./components/configurations/checklist/ChecklistListing.vue'));
Vue.component('checklist-flow', require('./components/configurations/checklist/ChecklistFlow.vue'));
Vue.component('checklist-delete', require('./components/configurations/checklist/DeletePopup.vue'));

// Incident Reporting
Vue.component('incident-reporting', require('./components/incident-reporting/IncidentReporting.vue'));
Vue.component('incident-report', require('./components/incident-reporting/IncidentReport.vue'));
// Manage Cases Listing and Form
Vue.component('cases-listing', require('./components/manage-cases-popups/CasesListing.vue'));
Vue.component('cases-form', require('./components/manage-cases-popups/CasesForm.vue'));
Vue.component('cases-popups', require('./components/manage-cases-popups/Main.vue'));



Vue.component('info-message', require('./components/common-components/InfoMessage.vue'));
Vue.component('stage-tracking', require('./components/common-components/StageTracking.vue'));
Vue.component('case-owner', require('./components/common-components/CaseOwner.vue'));
Vue.component('reported-by', require('./components/common-components/ReportedBy.vue'));



Vue.component('complete-sanction', require('./components/common-components/CompleteSanction.vue'));
Vue.component('confirm-alert', require('./components/common-components/ConfirmAlert.vue'));
Vue.component('common-multiselect', require('./components/common-components/CommonMultiSelect.vue'));
Vue.component('approval-request', require('./components/popups/ApprovalRequest.vue'));

// Export Report
Vue.component('export-current-situation', require('./components/reports/export/ExportReportCurrentSituationPDF.vue'));
Vue.component('export-overall-stats', require('./components/reports/export/ExportReportOverAllStatsPDF.vue'));
Vue.component('export-report-popup', require('./components/popups/ExportReportPopup.vue'));