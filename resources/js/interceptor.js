import Vue from 'vue';
import Toasted from 'vue-toasted';
import VeeValidate from 'vee-validate'
let veeCustomMessage = {
    en: {
        custom: {
            agree: {
                required: 'You must agree to the terms and conditions before registering!',
                digits: (field, params) => `length must be ${params[0]}`
            },
            privacypolicy: {
                required: 'You must agree the privacy policy before registering!',
                digits: (field, params) => `length must be ${params[0]}`
            },
            password_confirmation: {
                confirmed: 'Password does not match.'
            },
        }
    }
};
const config = {
    errorBagName: 'errorBag', // change if property conflicts.
    dictionary:  veeCustomMessage,
    events: 'input' 
};

Vue.use(VeeValidate,config);


Vue.use(Toasted);

const app = new Vue({
});


Vue.axios.interceptors.response.use((response) => { // intercept the global error
    if (response.data.message && response.data.message != 'Success.') {
        app.$toasted.show(response.data.message, {
            theme: "bubble",
            position: "bottom-right",
            duration : 3000,
            type: ['success'],
            singleton: true,
            icon : '',
            className: 'success-toast alert-success',
        })
        response.data.message = ''
    }
    return response
}, function (error) {
    let originalRequest = error.config
    if (error.response.status === 401 && !originalRequest._retry) { // if the error is 401 and hasent already been retried
            app.$auth.logout().then(function (Vue) {

                app.$store.commit('setAuthUser', '')
                router.push({ name: 'login'})
            })
    }

    var status = error.response.status;
    if(status === 406 || status === 422 || status == 404){
        let errors = error.response.data.errors;

        _.forEach(errors, function(value, key) {

            app.$toasted.show(errors[key][0], {
                theme: "bubble",
                position: "bottom-right",
                duration : 3000,
                type: ['error'],
                singleton: true,
                icon : '',
                className: 'error-toast alert-danger'
            })
            return false
        });

        error.response.data.errors = []

        return Promise.reject(error);
    }
})