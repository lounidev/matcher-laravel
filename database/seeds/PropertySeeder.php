<?php

use Illuminate\Database\Seeder;
use App\Models\Property;
use Carbon\Carbon;

class PropertySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {       
        Property::truncate();
        $price = ["500000", "600000", "700000", "800000", "900000", "1000000", "1100000", "1200000", "1300000", "1400000", "1500000"];
        $area = ["40", "60", "80", "100", "120", "140", "160", "180", "200", "220", "240"];
        $yearOfConstruction = ["2010","2011","2012","2013","2014","2015","2016","2017","2018","2019","2020"];
        $rooms = ["1","2","3","4","5","1","2","3","4","5","5"];
        $heatingType = ["gas","gas","gas","gas","gas","gas","gas","gas","gas","gas","gas"];
        $parking = [true,true,true,false,true,false,true,true,true,true,true];
        $returnActual = ["10","11","12.8","12","12.8","10.5","13","15","11","12.8","12.8"];

        $size = 11;
        $data = [];
        $datetime = Carbon::now()->toDateTimeString();
        for($i = 0; $i < $size; $i++) {
            $data[$i] = [
                "name" => "House " . ($i + 1),
                "address" => "Address " . ($i + 1),
                "propertyType" => $i+1,
                "fields" => [
                    "price" => $price[$i],
                    "area" => $area[$i],
                    "yearOfConstruction" => $yearOfConstruction[$i],
                    "rooms" => $rooms[$i],
                    "heatingType" => $heatingType[$i],
                    "parking" => $parking[$i],
                    "returnActual" => $returnActual[$i],
                ],
                'created_at' => $datetime,
                'updated_at' => $datetime
            ];
        }

        Property::insert($data);
    }
}
