<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Configurations for uploaded content
    |--------------------------------------------------------------------------
    |
    */
    'ir' => [
        'folder' => 'ir/orignal',
        'rules' => [
            // 'mimes:jpg,jpeg,png,bmp,mp3,mp4,xls,docx,doc,pdf,ppt,xlsx,pptx,mpga,txt',
            'max:20000',
        ],
        'url' => [
            'folder' => 'ir'
        ]
    ],
    'data_import' => [
        'folder' => 'data_import/orignal/',
        'rules' => [
            'mimes:csv,txt',
            'max:100000',
        ],
    ],
    'service_provider' => [
        'folder' => 'service_provider/orignal',
        'rules' => [
            'mimes:jpeg,jpg,png,xlsx,xls,doc,docx,pdf',
            // 'max:' . 10*1024*1024,
        ],
        
        /*'thumb' => [
            'folder' => 'service/thumb/',
            'height' => 400,
            'width' => 400,
            'method' => 'fit',
        ],*/
        /*'zip' => [
            'folder' => 'service/zip/',
        ],*/
        'url' => [
            'folder' => 'service_provider'
        ]
    ],
    
    'user' => [
        'folder' => 'user/orignal',
        'rules' => [
            'mimes:jpeg,jpg,png',
        ],
        
    ],
    'job' => [
        'folder' => 'job/orignal',
        'rules' => [
            'mimes:jpeg,jpg,png',
        ],
        'thumb' => [
            'folder' => 'job/thumb/',
            'height' => 400,
            'width' => 400,
            'method' => 'fit',
        ],
        
    ],
    'attachment' => [
        'folder' => 'attachment/orignal',
        'rules' => [
            'mimes:jpg,jpeg,png,bmp,mp3,mp4,xls,docx,doc,pdf,ppt,xlsx,pptx,mpga',
            'max:20000',
            'min:0',
        ],
        // 'url' => [
        //     'folder' => 'attachment'
        // ]
    ],
    'email_attachment' => [
        'folder' => 'email_attachment/orignal',
        'rules' => [
            'mimes:jpg,jpeg,png,bmp,mp3,mp4,xls,docx,doc,pdf,ppt,xlsx,pptx,mpga',
            'max:20000'
        ],
    ],

    'logo' => [
        'folder' => 'logo/orignal',
        'rules' => [
            'mimes:jpg,jpeg,png,bmp',
        ]
    ],

    'email_pdf_attachment' => [
        'folder' => 'email_pdf_attachment/original',
    ],

    'sanction_attachment' => [
        'folder' => 'sanction_attachment/original',
        'rules' => [
            'mimes:jpg,jpeg,png,bmp,mp3,mp4,xls,docx,doc,pdf,ppt,xlsx,pptx,mpga',
        ],
    ],

    'meeting_icalendar' => [
        'folder' => 'meeting_icalendar/original',
    ],

    'scheduled_report' => [
        'folder' => 'scheduled_report/original',
    ],

    'export_report' => [
        'folder' => 'export_report',
    ],
];
