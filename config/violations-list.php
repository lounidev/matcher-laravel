<?php 


return [
    'data' => [
        [
            'key' => 'violations',
            'name' => 'Violation'
        ],
        [
            'key' => 'sanctions',
            'name' => 'Sanction'
        ],
        [
            'key' => 'fines',
            'name' => 'Fine'
        ],
        [
            'key' => 'appeal_types',
            'name' => 'Appeal Type'
        ],
        [
            'key' => 'appeal_statuses',
            'name' => 'Appeal Status'
        ],
        [
            'key' => 'appeal_review_types',
            'name' => 'Appeal Review'
        ],
        [
            'key' => 'appeal_outcomes',
            'name' => 'Appeal Outcome'
        ],
        [
            'key' => 'plea_types',
            'name' => 'Plea Type'
        ],
        [
            'key' => 'titleix_categories',
            'name' => 'Title IX'
        ],
        [
            'key' => 'hearing_types',
            'name' => 'Hearing Type'
        ],
        [
            'key' => 'hearing_locations',
            'name' => 'Hearing Location'
        ]
    ]
];

