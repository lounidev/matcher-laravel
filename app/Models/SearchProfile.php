<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class SearchProfile extends Model
{
    protected $connection = 'mongodb';
}
