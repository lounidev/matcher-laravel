<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Data\Models\Role;
use App\Data\Models\User;
use App\Data\Models\DisplaySetting;

use Carbon\Carbon;

class InitialSetup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'initial:setup {title} {email} {password} {primary_color} {secondary_color}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Configure initial setup';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $title = $this->argument('title');

        $email = $this->argument('email');
        $password = $this->argument('password');

        $primary_color = $this->argument('primary_color');
        $secondary_color = $this->argument('secondary_color');

        $date = Carbon::now();

        $user                = new User;
        $user->first_name    = "Super";
        $user->middle_name   = "";
        $user->last_name     = "Admin";
        $user->email         = $email;
        $user->password      = bcrypt($password);
        $user->role_id       = Role::SUPERADMIN;
        $user->status        = User::ACTIVE;
        $user->activated_at  = $date;
        $user->created_at    = $date;
        $user->updated_at    = $date;
        $user->profile_image = 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/328820/profile/profile-512.jpg?1';

        if ($user->save()) {
            $data = [
                'name' => $title,
                'logo' => 'logo.png',
                'primary_color' => $primary_color,
                'secondary_color' => $secondary_color,
                'ir_page' => [    
                    'title' => 'Student Conduct Incident Reporting Form',
                    'sub_title' => 'The reporting form is NOT a 911 or Emergency Service',
                    'details' => 'Do not use this site to report events,presenting an immediate threat safety or security threat.If this is an emergency, please contact UMKC Campus Safety & Police at +1 816-235-1000 or dial 911.',
                    'thank_you_title' => 'Thank you for you submission',
                    'thank_you_details' => 'This is confirmation that you have sucessfully submitted this incident

For record your incident report number is [[NEW_IR_NUMBER]]'
                ],
                'created_at' => $date,
                'updated_at' => $date,
                'deleted_at' => NULL,
            ];
            return DisplaySetting::create($data);
        }
    }
}
