<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Data\Models\ScheduledReport;
use App\Jobs\SendScheduledReportJob;
use Carbon\Carbon;


class SendScheduledReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sendReport:send-scheduled-report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send scheduled report to users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $now = Carbon::now();
        $currentTime = Carbon::now()->format('H:i:00');
        $dayOfTheWeek = Carbon::now()->dayOfWeek;
        \Log::info('Current Time');
        \Log::info($currentTime);

        $reports = ScheduledReport::where(function($query) use ($currentTime) {
          $query->where('formatted_time', '=', $currentTime);
          $query->orWhere('status', '!=', 'complete');  
        })->get();

        \Log::info('Cron reports');
        \Log::info(json_encode($reports));


        foreach ($reports as $key => $report) {
        \Log::info(json_encode($report->_id));

            $createdAt = Carbon::parse($report->created_at);


            \Log::info('Report type : ');
            \Log::info($report->repeat);


            \Log::info('Created at : ');
            \Log::info($createdAt);
            ScheduledReport::where('_id', $report->_id)->update(['status'=>'pending', 'in_pending_at' =>  Carbon::now()->toDateTimeString()]);
            \Log::info('report->repeat');
            \Log::info($report->repeat);
            if($report->repeat == 'week'){
                $sendReport = in_array($dayOfTheWeek , array_keys($report->days, true));
                \Log::info('in week');
                // \Log::info($report->repeat);
                if($sendReport){
                    \Log::info('if send report');
                    \Log::info($report);
                    SendScheduledReportJob::dispatch($report->_id)->onQueue(config('queue.prefix') . 'schedule-report-cron');
                }
            }
            
            if($report->repeat == 'two_weeks'){
                if(!($now->diffInWeeks($createdAt) % 2)){
                    $sendReport = in_array($dayOfTheWeek , array_keys($report->days, true));
                    if($sendReport){
                        SendScheduledReportJob::dispatch($report->_id)->onQueue(config('queue.prefix') . 'schedule-report-cron');             
                    }
                }
            }
            
            if($report->repeat == 'month'){
                if($now->diffInMonths($createdAt) <= 1){
                    if($report->date == $now->day){
                        SendScheduledReportJob::dispatch($report->_id)->onQueue(config('queue.prefix') . 'schedule-report-cron');
                    }
                }    
            }
        }
    }
}
