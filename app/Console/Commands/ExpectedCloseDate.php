<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Data\Models\IncidentCase;
use App\Data\Models\User;
use Carbon\Carbon;
use App\Data\Models\Role;
use App\Jobs\ExpectedCloseDateJob;

class ExpectedCloseDate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'expected:closeDate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send an email when case expected close date is near.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tomorrow = Carbon::tomorrow()->toDateString();

        $cases = IncidentCase::where('expected_close_date','=',$tomorrow)->get(['assigned_to','incident_type_id','case_number']);

        \Log::info('Tomorow');
        \Log::info($tomorrow);
        \Log::info('Cases');
        \Log::info($cases);
        
        $assigned_to = array();
        $incident_type_id = [];
        $incident_type_admin = [];
        $admin = [];
        $users = [];

        foreach ($cases as $key => $user) {
               $assigned_to[$user->case_number] = $user->getAttributes()['assigned_to'];
               $incident_type_id[$user->case_number] = $user->getAttributes()['incident_type_id'];
               $id[$user->case_number] = $user->getAttributes()['_id'];
            
        }

        foreach ($incident_type_id as $key => $value) {
            $incident_type_admin[$key]    = User::join('user_incident_types', function ($join) use ($value){
                                $join->on('user_incident_types.user_id', '=', 'users.id')
                                ->whereNotIn('user_incident_types.role_id', [Role::STUDENT])
                                 ->where('user_incident_types.incident_type_id', '=', $value)
                                ->whereNull('user_incident_types.deleted_at');
                            })->pluck('users.id')->toArray();
        }
        $admin = User::whereIn('role_id',[Role::SUPERADMIN])
        ->pluck('id')->toArray();
        
        foreach ($cases as $key => $value) {

                $users[$key]['case_number'] = $value->case_number;
                $users[$key]['case_id'] = $value->_id;
                $users[$key]['users'] = array_merge(array ($assigned_to[$value->case_number]),
                $incident_type_admin[$value->case_number],$admin);
        }
            ExpectedCloseDateJob::dispatch($users)->onQueue(config('queue.prefix') . 'expected-close-date-cron');
    }
}
