<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Email 
Route::get('/mailable', function () {
    $General = [];
    return new App\Mail\General($General);
});

Route::get('/clear', function(){
    \Cache::flush();
});

Route::get('/export-case-pdf-download/{name}', function($name) {
    $downloadUrl = storage_path('app/public/').config('uploads.export_report.folder').'/'.$name;
    return response()->download($downloadUrl)->deleteFileAfterSend();
});

Route::get('/', function () {
    return view('welcome');
});

Route::get('report-export/{id}',  'Api\V1\FileExportController@exportReport');

Route::get('/{any}', function(){
    return view('layout');
})->where('any', '.*');

Route::get('/', function(){
    return view('layout');
})->where('any', '.*');

