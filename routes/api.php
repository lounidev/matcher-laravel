<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
return $request->user();
});*/
//Auth Route
Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'Auth\LoginController@login');
    Route::post('login/admin', 'Auth\LoginController@adminLogin');
});

Route::resource('boardmembertypeuser', 'Api\V1\BoardMemberTypeUserController')->except([
    'edit'
]);

Route::resource('display-setting', 'Api\V1\DisplaySettingController')->only([
    'index'
]);

// used in Guest IRForm
Route::resource('involved-user-type-guest', 'Api\V1\InvolvedUserTypeController')->only([
    'index'
]);

Route::resource('location-guest', 'Api\V1\LocationController')->only([
    'index'
]);

Route::resource('specific-location-guest', 'Api\V1\SpecificLocationController')->only([
    'index'
]);

Route::resource('incident-type-guest', 'Api\V1\IncidentTypeController')->only([
    'index'
]);

Route::resource('incident-case-guest', 'Api\V1\IncidentCaseController')->only([
    'store'
]);

Route::resource('ir-form-guest', 'Api\V1\IrFormController')->only([
    'index', 'show'
]);

Route::post('file/upload', 'Api\V1\FileUploadController@upload')->name("file.upload");

Route::post('file/remove', 'Api\V1\FileUploadController@remove')->name("file.remove");

Route::post('file/custom-remove', 'Api\V1\FileUploadController@customRemove');

Route::resource('contact-support-guest', 'Api\V1\ContactSupportController')->except([
    'edit'
]);

Route::get('contact-support-reasons', 'Api\V1\ContactSupportController@getReasons')->name("support.reasons");
Route::get('violations-list', function(){
    return config('violations-list');
});

