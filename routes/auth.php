<?php


Route::group(['prefix' => 'auth'], function () {  
    Route::put('change/password', 'Api\V1\UserController@changePassword');
    Route::post('logout', 'Auth\LoginController@logout');
});



Route::group(['middleware' => ['scopes']], function () {


    Route::put('user/change-status/{id}', 'Api\V1\UserController@changeStatus')->name('change.status');

    Route::resource('case-meeting', 'Api\V1\CaseMeetingController')->only([
        'index', 'store', 'update'
    ]);

    Route::get('case-meeting/get-involved-users/{id}', 'Api\V1\CaseMeetingController@getInvolvedUsers');


    Route::post('case-move-mail', 'Api\V1\IncidentCaseController@caseStageChangeMail')->name('case-move.store');

    Route::resource('case-type-stage', 'Api\V1\CaseTypeStageController')->except([
        'edit'
    ]);

    Route::resource('checklist', 'Api\V1\ChecklistController')->only([
        'index', 'update', 'store', 'destroy'
    ]);

    Route::resource('charge', 'Api\V1\ChargeController')->only([
        'index', 'store', 'update'
    ]);
    Route::delete('charge/{id}', 'Api\V1\ChargeController@delete');

    Route::resource('case-plea', 'Api\V1\CasePleaController')->only([
        'index', 'store', 'update'
    ]);
    Route::delete('case-plea/{id}', 'Api\V1\CasePleaController@delete');

    Route::resource('incident-type', 'Api\V1\IncidentTypeController')->except([
        'edit','create','destory'
    ]);

    Route::resource('incident-case-clery-act', 'Api\V1\IncidentCaseCleryActController')->only([
        'index', 'update'
    ]);

    Route::resource('incident-case-other-detail', 'Api\V1\IncidentCaseOtherDetailController')->only([
        'index', 'update'
    ]);

    Route::resource('note', 'Api\V1\NotesController')->only([
        'index', 'store'
    ]);


    Route::resource('email-template', 'Api\V1\EmailTemplateController')->only([
        'index', 'store', 'update', 'destroy'
    ]);

    Route::resource('activity-log', 'Api\V1\ActivityLogController')->only([
        'index', 'store', 'update'
    ]);

    Route::get('stage/stage-list/{id}', 'Api\V1\StageController@getStageList')->name('stage.stage-list');
    Route::resource('stage', 'Api\V1\StageController')->except([
        'edit','create','store'
    ]);

    Route::resource('display-setting', 'Api\V1\DisplaySettingController')->only([
        'store'
    ]);


    Route::resource('violation', 'Api\V1\ViolationController')->only([
        'index', 'store', 'update'
    ]);

    Route::resource('plea-outcome', 'Api\V1\PleaOutcomeController')->only([
        'index', 'store', 'update'
    ]);

    Route::resource('plea-type', 'Api\V1\PleaTypeController')->only([
        'index', 'store', 'update'
    ]);

    Route::resource('sanction', 'Api\V1\SanctionController')->only([
        'index', 'store', 'update'
    ]);

    Route::resource('fine', 'Api\V1\FineController')->only([
        'index', 'store', 'update'
    ]);

    Route::resource('title-ix-category', 'Api\V1\TitleixCategoryController')->only([
        'index', 'store', 'update'
    ]);

    Route::resource('board-member-type', 'Api\V1\BoardMemberTypeController')->only([
        'index', 'store', 'update'
    ]);

    Route::resource('appeal-outcome', 'Api\V1\AppealOutcomeController')->only([
        'index', 'store', 'update'
    ]);

    Route::resource('appeal-type', 'Api\V1\AppealTypeController')->except([
        'edit'
    ]);

    Route::resource('appeal-status', 'Api\V1\AppealStatusController')->only([
        'index', 'store', 'update'
    ]);

    Route::resource('appeal-review-type', 'Api\V1\AppealReviewTypeController')->only([
        'index', 'store', 'update'
    ]);

    Route::resource('appeal', 'Api\V1\AppealController')->only([
        'index', 'store', 'update'
    ]);

    Route::get('appeal-update', 'Api\V1\AppealController@updateAppeal')->name('case-appeal.update-appeal');

    Route::resource('board-member-types', 'Api\V1\BoardMemberTypeController')->only([
        'index', 'store', 'update'
    ]);

    Route::resource('hearing-type', 'Api\V1\HearingTypeController')->only([
        'index', 'store', 'update'
    ]);

    Route::resource('hearing-location', 'Api\V1\HearingLocationController')->only([
        'index', 'store', 'update'
    ]);

    Route::resource('student-flag', 'Api\V1\StudentFlagController')->only([
        'index', 'store', 'update'
    ]);

    Route::get('incident-case/document-download-logs', 'Api\V1\IncidentCaseController@documentDownloadLogs')->name('incident.create-documents-log');

    Route::resource('incident-case', 'Api\V1\IncidentCaseController')->only([
       'index', 'store', 'update', 'show',
   ]);
    
    Route::put('incident-case/{id}/update-stage', 'Api\V1\IncidentCaseController@updateStage')->name('case.stage.update');
    Route::put('incident-case/{id}/change-owner', 'Api\V1\IncidentCaseController@changeOwner')->name('case.owner.update');
    Route::put('incident-case/{id}/discard-case', 'Api\V1\IncidentCaseController@discardCase')->name('case.discard.update');

    Route::put('incident-case/{id}/close-case', 'Api\V1\IncidentCaseController@closeCase')->name('case.close.update');

    Route::put('incident-case/{id}/reopen-case', 'Api\V1\IncidentCaseController@reopenCase')->name('case.reopen.update');
    Route::put('incident-case/{id}/expected-date', 'Api\V1\IncidentCaseController@expectedCloseDate')->name('case.expected-date.update');

    Route::get('user/get-users-to-list/{id}', 'Api\V1\UserController@getUsersToList')->name('get.user.list');

    Route::put('incident-case/add-involved-parties/{id}', 'Api\V1\IncidentCaseController@addInvolvedParties')->name('incident.addInvolvedParties');

    Route::put('incident-case/delete-involved-parties/{id}', 'Api\V1\IncidentCaseController@deleteInvolvedParties')->name('incident.deleteInvolvedParties');

    Route::resource('incident-case-collection', 'Api\V1\IncidentCaseCollectionController')->only([
        'index','update'
    ]);

    Route::put('incident-case/flag-involved-parties/{id}', 'Api\V1\IncidentCaseController@flagInvolvedParties')->name('incident.flagInvolvedParties');

    Route::put('incident-case/assign-proper-user/{id}', 'Api\V1\IncidentCaseController@assignProperUser')->name('incident.assign.proper.user');

    Route::put('incident-case/assign-proper-reporter/{id}', 'Api\V1\IncidentCaseController@assignProperReporter')->name('incident.assign.proper.reporter');

    Route::put('incident-case/add-title-ix/{id}', 'Api\V1\IncidentCaseController@addTitleIx')->name('incident.add.related.case');
    Route::put('incident-case/remove-title-ix/{id}', 'Api\V1\IncidentCaseController@removeTitleIx')->name('incident.remove.related.case');
    Route::get('incident-case/list-title-ix/{id}', 'Api\V1\IncidentCaseController@listTitleIx')->name('incident.list.related.case');


    Route::put('incident-case/add-related-case/{id}', 'Api\V1\IncidentCaseController@addRelatedCase')->name('incident.add.related.case');
    Route::put('incident-case/remove-related-case/{id}', 'Api\V1\IncidentCaseController@removeRelatedCase')->name('incident.remove.related.case');
    Route::get('incident-case/list-related-case/{id}', 'Api\V1\IncidentCaseController@listRelatedCase')->name('incident.list.related.case');


    Route::put('incident-case/case-at-risk/{id}', 'Api\V1\IncidentCaseController@caseAtRisk')->name('incident.case.at.risk');



    Route::resource('incident-case-collection', 'Api\V1\IncidentCaseCollectionController')->only([
        'update', 'show'
    ]);

    Route::get('user/notification', 'Api\V1\UserController@getUserNotification')->name('user.notification');

    Route::get('user/unread-notification-count', 'Api\V1\UserController@getUnreadNotificationCount')->name('user.unread.notification.count');
    Route::post('user/mark-read-notification', 'Api\V1\UserController@markRead')->name('user.notification.mark.read');
    Route::resource('user', 'Api\V1\UserController')->only([
        'update', 'store', 'index', 'show'
    ]);

    Route::resource('user-profile', 'Api\V1\UserProfileController')->except([
        'edit'
    ]);
    Route::resource('violation', 'Api\V1\ViolationController')->only([
        'index', 'store', 'update'
    ]);

    Route::resource('plea-type', 'Api\V1\PleaTypeController')->only([
        'index', 'store', 'update'
    ]);


    Route::resource('form-builder', 'Api\V1\FormBuilderController')->only([
        'index', 'store', 'update'
    ]);

    Route::resource('ir-form', 'Api\V1\IrFormController')->except([
        'edit'
    ]);

    Route::resource('user-incident-type', 'Api\V1\UserIncidentTypeController')->except([
        'edit'
    ]);
    Route::post('user-incident-type', 'Api\V1\UserIncidentTypeController@addOrUpdate');

    Route::resource('role', 'Api\V1\RoleController')->except([
        'edit'
    ]);

    Route::resource('case-sanction', 'Api\V1\CaseSanctionController')->only([
        'index', 'store', 'update'
    ]);
    
    Route::get('case-sanction-get-sanction-details', 'Api\V1\CaseSanctionController@getSanction')->name('case-sanction.get-sanction-details');

    Route::put('case-sanction-update-sanction-status/{id}', 'Api\V1\CaseSanctionController@updateSanctionStatus')->name('case-sanction.update-sanction-status');

    Route::delete('case-sanction/{id}', 'Api\V1\CaseSanctionController@delete');

    Route::post('board-users/{id}', 'Api\V1\BoardMemberTypeController@addMembers');

    Route::resource('student-note', 'Api\V1\StudentNoteController')->only([
        'index', 'store', 'update'
    ]);

    Route::get('board-type-users', 'Api\V1\BoardMemberTypeUserController@boardMembers');


    Route::resource('case-email', 'Api\V1\CaseEmailController')->only([
      'index', 'store', 'update'
    ]);


    Route::post('split-case', 'Api\V1\SplitCaseController@store')->name("split.case");

    Route::resource('user-custom-field', 'Api\V1\UserCustomFieldController')->only([
        'index', 'store', 'update', 'destroy'
    ]);
    Route::resource('cleryact-custom-field', 'Api\V1\CleryActCustomFieldController')->only([
        'index', 'store', 'update', 'destroy'
    ]);
    Route::resource('case-other-detail-custom-field', 'Api\V1\CaseOtherDetailCustomFieldController')->only([
        'index', 'store', 'update', 'destroy'
    ]);
    Route::resource('attachment', 'Api\V1\AttachmentController')->only([
        'index', 'store'
    ]);

    Route::resource('case-email', 'Api\V1\CaseEmailController')->only([
        'index', 'store'
    ]);

    Route::post('merge-case', 'Api\V1\MergeCaseController@store')->name("merge.case");
    
    Route::resource('involved-user-type', 'Api\V1\InvolvedUserTypeController')->only([
        'index', 'store', 'update'
    ]);

    Route::resource('location', 'Api\V1\LocationController')->only([
        'index', 'store', 'update'
    ]);

    Route::resource('specific-location', 'Api\V1\SpecificLocationController')->only([
        'index', 'store', 'update'
    ]);

    Route::resource('incident-type', 'Api\V1\IncidentTypeController')->except([
        'edit','create','destory'
    ]);


    Route::resource('incident-case', 'Api\V1\IncidentCaseController')->only([
        'index', 'store', 'update', 'show'
    ]);

    Route::resource('location', 'Api\V1\LocationController')->only([
        'index', 'store', 'update'
    ]);

    Route::get('system-log/action-list', 'Api\V1\SystemLogController@getActionsList')->name('system-log.actions');
    Route::get('system-log/load-graph', 'Api\V1\SystemLogController@getLoadGraph')->name('system-log.load-graph');
    Route::resource('system-log', 'Api\V1\SystemLogController')->except([
        'edit'
    ]);

    Route::resource('contact-support', 'Api\V1\ContactSupportController')->except([
             'edit'
    ]);
    
    Route::get('report-stat/current-situation', 'Api\V1\ReportStatsController@getCasesStatusForStats')->name('report-stat.get-cases-status-for-stats');
    
    Route::resource('report-stat', 'Api\V1\ReportStatsController')->only([
        'index'
    ]);

    Route::resource('export-case', 'Api\V1\ExportCaseController')->only([
        'store'
    ]);

    Route::post('export-case-pdf', 'Api\V1\ExportCasePdfController@getExportCasePdf');

    

    Route::get('custom-report/student-entities', 'Api\V1\ReportController@getStudentEntities')->name('custom-report.student-entities');
    Route::get('custom-report/case-entities', 'Api\V1\ReportController@getCaseEntities')->name('custom-report.case-entities');
    Route::resource('report', 'Api\V1\ReportController');
    Route::resource('schedule-report', 'Api\V1\ScheduledReportController');
    
    // Data Importer
    Route::post('data-import', 'Api\V1\DataImportController@import');
    Route::get('data-import', 'Api\V1\DataImportController@getTemplateUrl');

});


